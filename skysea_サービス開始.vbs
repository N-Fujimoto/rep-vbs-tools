'≪32bit OSの場合≫
const x86Cmd="""C:\Program Files\Sky Product\SKYSEA Client View\ForceStopAndResumer.exe"""
'≪64bit OSの場合≫
const x64Cmd="""C:\Program Files (x86)\Sky Product\SKYSEA Client View\ForceStopAndResumer.exe"""

private strMode,wsh,cmd

Set wsh = CreateObject("WScript.Shell")
strMode = wsh.Environment("Process").Item("PROCESSOR_ARCHITECTURE")

If UCase(strMode) = "X86" Then
	cmd = x86Cmd
else
	cmd = x64Cmd
end if

'dim fso
'set fso = createobject("Scripting.FileSystemObject")
'if fso.FileExists(replace(cmd,"""","")) then
''if fso.FileExists(cmd) then
'	msgbox cmd,vbInformation,"OK"
'else
'	msgbox cmd,vbCritical,"Failed"
'end if
'set fso = nothing

'ウインドウ非表示、実行終了待ち
call wsh.Run(cmd, 0, True)

set wsh = Nothing
