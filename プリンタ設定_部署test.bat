@echo off
:: ---------------------------------------------------------------------------::
:: ネットワークプリンタ設定
::   2016/06/02  N-FUJIMOTO@MEC 
::
:: [注意]
::   1. バッチは管理者権限で実行する必要があります
::   2. プリンタに割り当てる[IP]を作成してください
::   3. 表示させる[プリンタ名]を作成してください
::   4. ドライバのインストーラをアクセスできる場所に設置してください
::   5. ドライバのiniファイルからプリンタドライバ名を調べてください
::   6. [設定開始]欄の内容を入力してください
:: ---------------------------------------------------------------------------::
:: 設定開始（入力する値群はここから）
:: ---------------------------------------------------------------------------::
set ip=192.168.50.15
set pn=Canon iR-ADV C5235（テスト）
set dn=Canon iR-ADV C5235/5240 LIPSLX
set in=\\matsusaka\OPEN\99_その他\A\winlipslxver2170x6402\x64\Driver\CNLB0JA64.INF
:: 以下必要であれば
:: プロトコル
set pc=raw
:: ポート
set po=9100
:: ---------------------------------------------------------------------------::
:: 設定終わり（ここまで）
:: ---------------------------------------------------------------------------::
:: win7用
cd C:\Windows\System32\Printing_Admin_Scripts\ja-JP
cscript /nologo prnport.vbs -a -s %COMPUTERNAME% -r IP_%ip% -h %ip% -o %pc% -n %po%
cscript /nologo prndrvr.vbs -a -m "%dn%" -v 3 -i "%in%"
cscript /nologo prnmngr.vbs -a -p "%pn%" -m "%dn%" -r IP_%ip%
:: echo /nologo prnport.vbs -a -s %COMPUTERNAME% -r IP_%ip% -h %ip% -o %pc% -n %po%
:: echo /nologo prndrvr.vbs -a -m "%dn%" -v 3 -i "%in%"
:: echo /nologo prnmngr.vbs -a -p "%pn%" -m "%dn%" -r IP_%ip%
:: @pause