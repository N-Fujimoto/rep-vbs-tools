'*********************************************************************************
'*
'*+ ieでグループウェアのスケジューラを1ステップで開くヤツ >>N-fujimoto@IT-ENGG
'*
'*********************************************************************************
'*
'*+ 自動ログインにするためのコマンドライン引数
'*
'*  ログインユーザ	：引数[-u]に続けて職員番号
'*  パスワード		：引数[-p]に続けてパスワード
'*
'*   例) 
'*    ログインユーザが「123456」,パスワードが「qwertyui」の場合は...
'*
'*    -u123456 -pqwertyui
'*
'*
'*+ 表示するスケジューラ設定
'*
'*   1 = 日表示
'*   2 = 週個人
'*   3 = 週グループ
'*   4 = 月表示
'*   5 = 空き時間
'*
'*  ※指定しない場合は月になります!
'*
	const sch = 3
'*
'*********************************************************************************
'*
'* 以降、変更箇所なし
'*
'*********************************************************************************
'定数
	const url = "http://gwsv.matsusaka.co.jp/next2mx/default.cfm"
	const qstr = "?version=demo&app_cd=33&fuseaction=%param%&date=%date%&gid=-1"
	const pday = "sch_day" 				'日表示(1)
	const pperson = "sch_person" 		'週個人(2)
	const pweek = "sch_week" 			'週グループ(3)
	const pmonth = "sch_month" 			'月表示(4)
	const ptmsearch = "sch_tmsearch" 	'空き時間(5)
	Const strIEexe = "iexplore.exe" 	'IEのプロセス名
on error resume next
	Dim IE  							'IE用変数
	set IE =  GetObject("new:{D5E8041D-920F-45e9-B8FB-B1DEB82C6E5E}")
	err = 999
	if err.number <> 0 then
		err.clear
		set IE = CreateObject("InternetExplorer.Application")   ' IE起動
		if err.number <> 0 then
			msgbox "インターネットエクスプローラーの起動に失敗しました.プロセスが重複していないか確認してください.",vbCritical
			Wscript.quit
		end if
	end if
on error goto 0
'変数
	Dim intProcID
'メイン
	call main()
'*********************************************************************************
'メイン
'*********************************************************************************
private sub main()
	dim msg
	dim dt
	dim urlpara
	dim ret
	dim strUserID
	dim strPassword
	dim i
	dim iCount
	dt = date()
	msg = inputbox("いつのスケジューラを参照しますか." & string(2,vbcrlf) & _
					"【日付形式】" & string(1,vbcrlf) & "　yyyy-mm-dd  yyyymmdd  yyyy/mm/dd" & string(5,vbcrlf) & _
					"　キャンセルで終了します.","スケジューラ検索",dt)
	'キャンセルで終了
	if msg = vbNullString then exit sub
	'日付型でないものは[yyyymmdd]型でないと受け付けないぜ
	if instr(msg,"/") = 0 and instr(msg,"-") = 0 and len(msg) = 8 then
		msg = left(msg,4) & "/" & mid(msg,5,2) & "/" & right(msg,2)
	end if
	'日付チェック
	if not isdate(msg) then
		call msgbox("入力されたデータは日付ではありません.",vbCritical,"日付チェックエラー")
		msg = vbNullString
		'再帰
		call main()
		exit sub
	end if
	'日付をハイフン編集に変える
	msg = year(msg) & "-" & month(msg) & "-" & day(msg)
	'引数を調査
	iCount = Wscript.Arguments.Count
	if iCount > 0 then
		for i = 0 to iCount - 1
			strArg = WScript.Arguments(i)
			if instr(strArg,"-u") > 0 then
				strUserID = replace(strArg,"-u","")
			elseif instr(strArg,"-p") > 0 then
				strPassword = replace(strArg,"-p","")
			end if
		next
	end if
	'設定未入力なら入力させる	
	if len(strUserID) = 0 then
		do 
			strUserID = inputbox("ユーザIDを入力してください." & string(6,vbcrlf) & _
									"※[空白]かキャンセルで終了します.","ユーザID未設定")
			ret = inputCheck(strUserID,"ユーザID")
			if ret = vbCancel then exit sub
		loop until ret
	end if
	if len(strPassword) = 0 then
		do 
			strPassword = inputbox("パスワードを入力してください." & string(6,vbcrlf) & _
									"※[空白]かキャンセルで終了します.","パスワード未設定")
			ret = inputCheck(strPassword,"パスワード")
			if ret = vbCancel then exit sub
		loop until ret
	end if
	'スケジューラの種類
	select case sch
		case 1: p = pday '日表示(1)
		case 2: p = pperson '週個人(2)
		case 3: p = pweek '週グループ(3)
		case 4: p = pmonth '月表示(4)
		case 5: p = ptmsearch '空き時間(5)
		case else
			p = pmonth 'デフォルトは月でいいかな?
	end select
	'urlを組み立てる
	para = replace(qstr,"%param%",p)
	para = replace(para,"%date%",msg)
	urlpara = url & para
	'ieで開く
	call use_ie(urlpara,strUserID,strPassword)
end sub
'*********************************************************************************
' IE用 Subプロシージャ
'*********************************************************************************
Private Sub use_ie(urlpara,uid,pass)
 	dim objForm

    IE.Navigate urlpara
    IE.Visible = True   ' IEの可視化
    
    call ActiveIE
    call waitIE(urlpara)	' IEの起動待機 2020/01/17  N-FUJIMOTO mod

	'ログインが必要かどうか調べる（いい方法がなくて）
	on error resume next
	dim objDoc
	objDoc = IE.document.getElementById("LoginUserID")

 	if err.number = 0 then
	 	with IE.Document
			'ログイン
			.getElementById("LoginUserID").Value = uid
			.getElementById("LoginPassword").Value = pass
			WScript.Sleep 500
			.all("LoginBtn").Click
		end with
	else
		err.clear
	end if
End Sub
'********************************************************************************* 
' IEがビジー状態の間待ちます
'*********************************************************************************
Private Sub waitIE(byval key)
    Do While IE.Busy = True Or IE.readystate <> 4
        WScript.Sleep 1000
    Loop
End Sub
'*********************************************************************************
'ウインドウをアクティブにする
'*********************************************************************************
Private sub ActiveIE()
    Dim objWshShell
    GetProcID(strIEexe)
    Set objWshShell = CreateObject("Wscript.Shell")
    objWshShell.AppActivate intProcID
    Set objWshShell = Nothing
End Sub
Private Function GetProcID(ProcessName)
    Dim Service
    Dim QfeSet
    Dim Qfe
    Set Service = WScript.CreateObject("WbemScripting.SWbemLocator").ConnectServer
    Set QfeSet = Service.ExecQuery("Select * From Win32_Process Where Caption='"& ProcessName &"'")
    intProcID = 0
    For Each Qfe in QfeSet
        intProcID = Qfe.ProcessId
        Exit For
    Next
    GetProcID = intProcID <> 0
End Function
'*********************************************************************************
'入力確認
'*********************************************************************************
Private function inputCheck(str,cap)
	dim ret
	if len(str) > 0 then
		ret = msgbox("入力した" & cap & "は..." & string(2,vbcrlf) & _
				"[" & str & "]" & string(2,vbcrlf) & " でよろしいですか.", _
				vbYesNoCancel,cap & "確認")
	end if
	if ret = vbyes then
		inputCheck = True
	elseif ret = vbNo then
		inputCheck = False
	else
		inputCheck = vbCancel
	end if
end function
