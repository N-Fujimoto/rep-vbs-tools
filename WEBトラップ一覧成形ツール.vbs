'============================================================
'SNMPトラップ、VMWareトラップの成形基礎
'2019/10/25  N-FUJIMOTO Ver0.9 エクセルブックの内容をコピーしたテキストファイルをCSVに成形する
'2019/11/01  N-FUJIMOTO Ver1.0 エクセルブックを直接読み、teratermマクロでファイル設置まで行う
'2020/01/22  N-FUJIMOTO Ver1.1 処理結果をメール送信するように変更
'2020/03/09  N-FUJIMOTO Ver1.2 メールのエンコード設定を追加
'2020/06/10  N-FUJIMOTO Ver1.4 前回ファイルを残すように変更 不要なソースを削除
'2020/06/19  N-FUJIMOTO Ver1.5 SlackのBotにも対応したことを通知
'2021/01/06  N-FUJIMOTO Ver1.6 アラート一覧2種を一括アップロードするためマージしたファイルを生成する
'2021/06/01  N-FUJIMOTO Ver1.8 ハードウェアセンサー、ストレージイベント追加
'配置場所：\\matsusaka\OPEN\10_部署\ITインフラ部\02_業務\A\@IaaS\01_運用(権限有)\10_OpManager
'============================================================
Option Explicit
Const excelPath = "\\matsusaka\OPEN\10_部署\ITインフラ部\02_業務\A\@IaaS\01_運用(権限有)\10_OpManager\"
Const excelFile = "SNMPトラップ発生時対応手順210430.xlsx"
Const trap1 = "snmp.txt"
Const trap2 = "vmware.txt"
Const trap3= "hardware.txt"	'2021/06/01  N-FUJIMOTO add
Const trap4 = "strage.txt"	'2021/06/01  N-FUJIMOTO add
Const out1 = "snmp.csv"
Const out2 = "vmware.csv"
Const out3= "hardware.csv"	'2021/06/01  N-FUJIMOTO add
Const out4 = "strage.csv"	'2021/06/01  N-FUJIMOTO add
Const outM ="TrapMerge.csv" '2021/01/06  N-FUJIMOTO add
Const brStr = "。 ." '改行コードとは別に改行制御にする文字の配列（半角SP区切り）
Const svName = "mmecvsv00223.matsusaka.co.jp"
Const svPath = "/var/www/naisen/data/"
Const ttm = "WEBトラップ一覧CSVアップロード.ttl"
Const strSmtp = "192.168.10.4"
Const intPort = 25
Const strMailFrom = "n-fujimoto@matsusaka.co.jp"
Const strMailTo = "it@matsusaka.co.jp"
'*** ↓ IE message ******************************************
'変数
Dim result
Dim IE, strMsg, intProcID
Set IE = CreateObject("InternetExplorer.Application")
result = 0 'エラーを発生させるなら↓を[1〜4]に変更する
'*** ↑ IE message ******************************************
'============================================================
'成形の注意点
'msgbox "【SNMP Trap】" & vbcrlf & _
'       "  最終行に[*]の行を追加してください." & vbcrlf & _
'       "  [UCS Manager/UCS]はダブルクォートを削除してください." & vbcrlf & _
'       "  [,]は全角[，]に置換してください." & vbcrlf & _
'       "【VMwareイベント】" & vbcrlf & _
'       "  最終行に[*]の行を追加してください." & vbcrlf & _
'       "  2重ダブルクォートを[***]に置換してください."
'開始
Call main

'*** ↓ IE message ******************************************
Set IE = Nothing
'*** ↑ IE message ******************************************

'============================================================
'メインルーチン
Private Sub main()
    Dim inp
    Dim out
    Dim strPath
    Dim strFile
    Dim strTxt1
    Dim strTxt2
    Dim strTxt3	'2021/06/01  N-FUJIMOTO add
    Dim strTxt4	'2021/06/01  N-FUJIMOTO add
'init
    strPath = excelPath
    strFile = excelFile
    strTxt1 = trap1
    strTxt2 = trap2
    strTxt3 = trap3	'2021/06/01  N-FUJIMOTO add
    strTxt4 = trap4	'2021/06/01  N-FUJIMOTO add
    Call viewWait(IE, result, "[ " & strTxt1 & " ] [ " & strTxt2 & " ] [ " & strTxt3 & " ] [ " & strTxt4 & " ] を作成しています...", "black")	'2021/06/01  N-FUJIMOTO mod
'exel to csv
    Call func3(strPath, strFile, strTxt1, strTxt2, strTxt3, strTxt4)	'2021/06/01  N-FUJIMOTO mod
    
'trap1
    inp = trap1
    out = out1
    Call viewWait(IE, result, "[ " & out & " ] を作成しています...", "black")
    Call func1(strPath, inp, out)

'trap2
    inp = trap2
    out = out2
    Call viewWait(IE, result, "[ " & out & " ] を作成しています...", "black")
    Call func1(strPath, inp, out)

'2021/06/01  N-FUJIMOTO add s
'trap3
    inp = trap3
    out = out3
    Call viewWait(IE, result, "[ " & out & " ] を作成しています...", "black")
    Call func1(strPath, inp, out)

'trap4
    inp = trap4
    out = out4
    Call viewWait(IE, result, "[ " & out & " ] を作成しています...", "black")
    Call func1(strPath, inp, out)
'2021/06/01  N-FUJIMOTO add e

    'msgbox     "[ " & out1 & " ][ " & out2 & " ]を" & vbcrlf & _
    '       "[ " & svName & "] の [" & svPath & "] に設置してください.", vbInformation,"【成形は完了しました】"

'CSVをマージしてインポートしやすく
	'call csvMerge(strPath,out1,out2) '2021/01/06  N-FUJIMOTO 
	call csvMerge(strPath,out1,out2,out3,out4) '2021/06/01  N-FUJIMOTO mod
''###################################################################################################################################
'	Call waitClose(IE, "終了しました.", 5) 'IE,メッセージ,秒
'	Wscript.Quit	'2021/06/01  N-FUJIMOTO test
''###################################################################################################################################
'teraterm マクロ
    Call viewWait(IE, result, "ファイルを転送しています...", "black")
    Call func4(strPath, ttm)

'処理結果をメール送信
	Call viewWait(IE, result, "メールを送信しています...", "black")
    Call sendMail(strPath, strFile)

'終了
    Call waitClose(IE, "終了しました.", 5) 'IE,メッセージ,秒
    
End Sub
'============================================================
'ファイル切り出しサブルーチン
Private Sub func3(ByVal strPath, ByVal strFile, ByVal strTxt1, ByVal strTxt2, ByVal strTxt3, ByVal strTxt4)	'2021/06/01  N-FUJIMOTO mod
    Dim xlsApp
    Dim fso
    Dim xlsFile
    Dim xlsSheet
    Dim out
    Dim outFile
    Dim i
    Dim l
    Dim k
    
    Set xlsApp = CreateObject("Excel.Application")
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    '2020/11/10  N-FUJIMOTO add s
    if Not (fso.FileExists(fso.BuildPath(strPath, strFile))) then
    	MsgBox "エクセルブック[ " & strFile & " ]がありませんでしたので終了します.",vbCritical,"エラー"
    	Call waitClose(IE, "終了しました.", 3)
    	WScript.quit
    end if
    '2020/11/10  N-FUJIMOTO add e
    
    Set xlsFile = xlsApp.Workbooks.Open(fso.BuildPath(strPath, strFile))
    
    For i = 1 To 4	'2021/06/01  N-FUJIMOTO mod
        Set xlsSheet = xlsApp.Worksheets.Item(i)
        If i = 1 Then out = strTxt1
        If i = 2 Then out = strTxt2
        If i = 3 Then out = strTxt3	'2021/06/01  N-FUJIMOTO add
        If i = 4 Then out = strTxt4	'2021/06/01  N-FUJIMOTO add
        'FileSystemObject.OpenTextFile(filename[, iomode[, create[, format]]])
        '0  ASCIIファイルとして開きます
        '-1 Unicodeファイルとして開きます
        '-2 システムの既定値で開きます
        Set outFile = fso.OpenTextFile(fso.BuildPath(strPath, out), 2, True, -1)
        l = 1
        ReDim arr(4)
        With xlsSheet
            Do Until .Cells(l, 1) = vbNullString
                For k = 1 To 5
                    arr(k - 1) = Replace(.Cells(l, k),",","，") '2021/01/07  N-FUJIMOTO mod
                Next
                outFile.WriteLine Join(arr, vbTab)
                l = l + 1
            Loop
        End With
        outFile.Close
    Next
    
    xlsApp.Quit()
    
    Set fso = Nothing

End Sub
'============================================================
'ファイル成形サブルーチン
Private Sub func1(ByVal strPath, ByVal inp, ByVal out)
'on error resume next
    Dim fso
    Dim strLine
    Dim inpFile
    Dim outFile
    Dim arr
    'dim i
    'dim l
    Dim strWork
    Dim bolBr
    'dim cntBr
    Dim brTable
    Dim strBre
    
    '改行文字列テーブル（タブ文字はハードコーディングで追加(^^;）
    brTable = Split(brStr)
    ReDim Preserve brTable(UBound(brTable))
    brTable(UBound(brTable)) = vbTab
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    'ファイルの存在チェック
    If Not (fso.FileExists(fso.BuildPath(strPath, inp))) Then
        MsgBox "トラップ一覧ファイル [" & inp & "] が設置されていません.", vbCritical
        WScript.Quit
	End If
    
    'FileSystemObject.OpenTextFile(filename[, iomode[, create[, format]]])
    '0  ASCIIファイルとして開きます
    '-1 Unicodeファイルとして開きます
    '-2 システムの既定値で開きます
    Set inpFile = fso.OpenTextFile(fso.BuildPath(strPath, inp), 1, False, -1)
    If Err.Number = 0 Then
    	'2020/06/08  N-FUJIMOTO add s
	    if fso.FileExists(fso.BuildPath(strPath, out)) then
	    	fso.CopyFile fso.BuildPath(strPath, out),DateFormat(Now()) & "_" &  out
		end if
		'2020/06/08  N-FUJIMOTO add e
        Set outFile = fso.OpenTextFile(fso.BuildPath(strPath, out), 2, True, -1)
        'タイトル読み飛ばし
        inpFile.SkipLine
        Do Until inpFile.AtEndOfStream
            '成形実施
            If bolBr Then
                arr = Split(strWork, vbTab)
                'IF ubound(arr) < 4 then Redim Preserve arr(4) '2020/12/17  N-FUJIMOTO 空欄対応
                If Not (arr(1) = vbNullString) Then
                    '対処方法を成形
                    arr(4) = func2(strWork)
                    '書き込み
                    outFile.WriteLine Join(arr, ",")
                End If
                '成形領域クリア
                strWork = vbNullString
                bolBr = False
            End If

            '1行読み込み
            strLine = inpFile.ReadLine
            arr = Split(strLine, vbTab)
            'IF ubound(arr) < 4 then Redim Preserve arr(4) '2020/12/17  N-FUJIMOTO 空欄対応
            
            '分割できるか
            If UBound(arr) > 0 Then
                '前レコードがあるか
                If Not (strBre = vbNullString) Then
                    strWork = strBre
                End If
                If strWork = vbNullString Then
                    '1行毎に改行させるため<BR>を付与
                    strWork = strBre & strWork & strLine & "<BR>"
                    strBre = vbNullString
                Else
                    bolBr = True
                    strBre = strLine
                End If
            Else
                '1行毎に改行させるため<BR>を付与
                strWork = strBre & strWork & strLine & "<BR>"
                strBre = vbNullString
            End If
        Loop
        
        If Not (strWork = vbNullString) Then
            arr = Split(strWork, vbTab)
            'IF ubound(arr) < 4 then Redim Preserve arr(4) '2020/12/17  N-FUJIMOTO 空欄対応
            If Not (arr(1) = vbNullString) Then
                '対処方法を置換
               	arr(4) = func2(strWork)
                '書き込み
                outFile.WriteLine Join(arr, ",")
            End If
        End If
        
        If Not (strBre = vbNullString) Then
            arr = Split(strBre, vbTab)
            'IF ubound(arr) < 4 then Redim Preserve arr(4) '2020/12/17  N-FUJIMOTO 空欄対応
            If Not (arr(1) = vbNullString) Then
                '対処方法を置換
                arr(4) = func2(strBre)
                '書き込み
                outFile.WriteLine Join(arr, ",")
            End If
        End If
        
        'outFile.WriteLine "*" '最終行

        inpFile.Close
        outFile.Close
        
        Call fso.DeleteFile(fso.BuildPath(strPath, inp), True)
        
    End If
    Set inpFile = Nothing
    Set outFile = Nothing
    Set fso = Nothing
    
End Sub
'============================================================
'編集
Private Function func2(ByVal strTxt)
    Dim strOut
    Dim strWork
    Dim arr
    Dim brTable
    Dim i
    Dim l
    
    '改行文字列テーブル（タブ文字はハードコーディングで追加(^^;）
    brTable = Split(brStr)
    ReDim Preserve brTable(UBound(brTable))
    brTable(UBound(brTable)) = vbTab

    'No トラップ名  重要度  発生元  対応方法
    'ダブルクォートを外す
    strWork = Replace(strTxt, Chr(34), vbNullString)
    arr = Split(strWork, vbTab)
    'IF ubound(arr) < 4 then Redim Preserve arr(4) '2020/12/17  N-FUJIMOTO 空欄対応
    '対処方法を成形
    
    strWork = arr(4)
    '1文字づつ判定
    For i = 1 To Len(strWork)
        strOut = strOut & Mid(strWork, i, 1)
        '最後の改行以外を<BR>とする
        'if i < len(strWork) then
            For l = LBound(brTable) To UBound(brTable)
                If Mid(strWork, i, 1) = brTable(l) Then
                    strOut = strOut & "<BR>"
                    Exit For
                End If
            Next
        'end if
    Next
    '重複<BR>をカット
    strOut = Replace(strOut, "<BR><BR>", "<BR>")
    '末尾の<BR>をカット
    If Right(strOut, 4) = "<BR>" Then
        strOut = Mid(strOut, 1, Len(strOut) - 4)
    End If
    func2 = strOut
End Function
'============================================================
'teratermマクロ
Private Sub func4(ByVal strPath, ByVal strMacro)
    Dim shl
    Dim fso
    
    Set shl = CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")
    '終了を待つ
    Call shl.Run(fso.BuildPath(strPath, strMacro), 1, True)
    
    Set shl = Nothing
    Set fso = Nothing

End Sub
'*** ↓ IE message ******************************************
'IEを開いてメッセージ表示
Private Sub viewWait(ByVal IE, ByVal result, ByVal msg, ByVal col)
    Dim strMsg
    'エラーの場合のメッセージを作る
    If result <> 0 Then
        strMsg = "ErrorCode:" & result & "<br><br>" & msg
    Else
        strMsg = msg
    End If
    With IE
        If .Visible = False Then
            .Navigate "about:blank"
            While .busy: Wend
            While .Document.readyState <> "complete": DoEvents: Wend
            .Document.body.innerHTML = "<span id='msg' style='color:" & col & "'>" & strMsg & "</span>"
            .AddressBar = False
            .Toolbar = False
            .StatusBar = False
            .Height = 150
            .Width = 300
            .Top = 0
            .Left = 0
            .Visible = True
        Else
            'IE.Document.getElementById("msg").innerHTML = msg
            .Document.getElementById("msg").innerHTML = "<span id='msg' style='color:" & col & "'>" & strMsg & "</span>"
        End If
    End With
    'IEを前面に
    Call GetProcID(IE)
    CreateObject("Wscript.Shell").AppActivate intProcID
End Sub
'****************************************************************************************
'IEにメッセージを表示してから指定秒後に削除する
Private Sub waitClose(ByVal IE, ByVal msg, ByVal T)
On Error Resume Next
    IE.Document.getElementById("msg").innerHTML = msg
    WScript.Sleep T * 1000
    IE.Quit
End Sub
'****************************************************************************************
Private Function GetProcID(ByVal ProcessName)
    Dim Service
    Dim QfeSet
    Dim Qfe
    
    Set Service = WScript.CreateObject("WbemScripting.SWbemLocator").ConnectServer
    Set QfeSet = Service.ExecQuery("Select * From Win32_Process Where Caption='" & ProcessName & "'")
 
    intProcID = 0
 
    For Each Qfe In QfeSet
        intProcID = Qfe.ProcessId
        Exit For
    Next
 
    GetProcID = intProcID <> 0
    
End Function
'****************************************************************************************
'** ファイルに書き込み処理
'**  str ログメッセージ
'**  path ファイルパス
'****************************************************************************************
Private Sub SysWriter(ByVal result, ByVal str, ByVal path)
 
    Dim fso, fi
    Set fso = CreateObject("Scripting.FileSystemObject")
     
    Set fi = fso.OpenTextFile(path, 8, True)
    
    If result = 0 Then
        str = Date & " " & Time() & ",〇," & str
    Else
        str = Date & " " & Time() & ",×," & str
    End If
    Call fi.WriteLine(str)
    Set fi = Nothing
 
End Sub
'*** ↑ IE message ******************************************

'****************************************************************************************
'** 処理結果をメール送信
'****************************************************************************************
Private sub sendMail(byval strPath, byval strFile)
	Dim objMail,fso,obj
	Set objMail = CreateObject("CDO.Message")
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	set obj = fso.GetFile(fso.BuildPath(strPath,strFile))
	
	with objMail
		'2020/03/09  N-FUJIMOTO add s
		'.TextBodyPart.Charset = "ISO-2022-JP"
		.BodyPart.Charset = "utf-8"
		'2020/03/09  N-FUJIMOTO add e
		.From = strMailFrom
		.To = strMailTo
		.Subject = "【連絡】SNMPトラップ対応手順の更新について"
		.TextBody = _
			"To IaaS担当" & string(2,vbcrlf) & _
			"□ 下記[SNMPトラップ対応Excel表]に更新がありました." & string(2,vbcrlf) & _
			"　ファイルパス：" & vbtab & excelPath & string(1,vbcrlf) & _
			"　ファイル名：" & vbtab & excelFile & string(1,vbcrlf) & _
			"　更新日時：" & vbtab & obj.DateLastModified & string(2,vbcrlf) & _
			"□ データ更新を実施しましたので,ご確認ください." & string(1,vbcrlf) & _
			vbtab & "http://fileex:18080/snmp.html" & string(2,vbcrlf) & _
			"□ Slack Bot (#opmq) への反映は実施予定です." & string(2,vbcrlf) & _
			"※ このメールは[WEBトラップ一覧成形ツール]から送信しました." & string(2,vbcrlf) & _ 
			"不明点は…[n-fujimoto@matsusaka.co.jp]まで." & string(1,vbcrlf)
		
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = strSmtp
		.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = intPort
		.Configuration.Fields.Update
		
		.Send
	
	end with
	
	Set objMail = Nothing
	set fso = Nothing
	
end sub

' 日付文字生成
private Function DateFormat(ByVal strDate)
     
    Dim strYear
    Dim strMonth
    Dim strDay
     
    strYear  = Year(strDate)
    strMonth = Right("0" & Month(strDate), 2)
    strDay   = Right("0" & Day(strDate), 2)
     
    DateFormat =  strYear & strMonth & strDay
     
End Function

' csvファイルをマージする
' 2021/01/06  N-FUJIMOTO
private sub csvMerge(byval strPath, byval inp1, byval inp2, byval inp3, byval inp4)	'2021/06/01  N-FUJIMOTO mod
	dim fso,inpFile,buf
	
	Set fso = CreateObject("Scripting.FileSystemObject")

	Set inpFile = fso.OpenTextFile(fso.BuildPath(strPath, inp1), 1, False, -1)
	buf = inpFile.ReadAll
	set inpFile = Nothing

	Set inpFile = fso.OpenTextFile(fso.BuildPath(strPath, inp2), 1, False, -1)
	buf = buf & inpFile.ReadAll
	set inpFile = Nothing

'2021/06/01  N-FUJIMOTO add s
	Set inpFile = fso.OpenTextFile(fso.BuildPath(strPath, inp3), 1, False, -1)
	buf = buf & inpFile.ReadAll
	set inpFile = Nothing
	
	Set inpFile = fso.OpenTextFile(fso.BuildPath(strPath, inp4), 1, False, -1)
	buf = buf & inpFile.ReadAll
	set inpFile = Nothing
'2021/06/01  N-FUJIMOTO add e

    With CreateObject("ADODB.Stream")
        .Charset = "UTF-8"
        .Open
        .WriteText buf
        .SaveToFile fso.BuildPath(strPath, outM), 2
        .Close
    End With
    
    set fso = Nothing
    
end sub