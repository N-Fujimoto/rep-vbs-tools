@echo off
rem ------------------------------------------------------------------
rem 複数ファイル対応
rem 2022/08/09  N-FUJIMOTO 
rem ------------------------------------------------------------------
rem 引数なしは終わり
if "%~1"=="" (
  echo 引数がありません
  pause
  exit /b
)
rem 引数ループ
:LOOP
	set arg=%1
	if "%arg%"=="" goto :eof
	call :GETEXT %arg%
	shift
goto :LOOP
rem ファイル確認と追記
:GETEXT
setlocal enabledelayedexpansion
	set BN=%~n1
	if "%~x1"==".csv" (
		set bol=FALSE
		rem echo %BN%
		rem echo %BN:~0,2%
		if "%BN:~0,2%"=="k_" set bol=TRUE
		if "%BN:~0,2%"=="h_" set bol=TRUE
		if "%BN:~0,2%"=="n_" set bol=TRUE
		rem echo !bol!
		rem pause
		if "!bol!"=="TRUE" (
		    set year=%date:~0,4%
		    set month=%date:~5,2%
		    set day=%date:~8,2%
		    set hour=%time:~0,2%
		    set minute=%time:~3,2%
		    set second=%time:~6,2%
		    set logname=UPDATE to %year%-%month%-%day%_%hour%-%minute%-%second%
		    echo %logname% >> %1
		)
	)
endlocal