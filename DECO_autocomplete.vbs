option explicit
'*********************************************************************************
'*
'*+ DECOをIEで実行して基本的な値を入力してくれるやつ
'*  Ver1.0	2019/05/09  N-FUJIMOTO 
'*
'*********************************************************************************
'*
'*+ 自動入力するためのコマンドライン引数
'*
'*  部署id					：引数[-i]に続けて部署id一覧のコード、なければエラー
'*  送信者名前				：引数[-a]に続けて入力、省略可
'*  送信者アドレス			：引数[-b]に続けてアカウント部分だけを入力、省略可
'*  ダウンロードパスワード	：引数[-p]に続けて入力、省略可
'*  相手へのメッセージ		：引数[-m]に続けて入力、省略可
'*  受取人�@名前			：引数[-x]に続けて入力、省略可
'*  受取人�@メールアドレス	：引数[-y]に続けてメールアドレスを入力、省略可
'*  フラッシュ未対応版		：引数[-f]に続けて1を入力、なければフラッシュ対応版となる
'*
'*   ショーカット作成例) 
'*    部署が[it]、送信者指名[太郎]、送信者アドレス[taro@exsample.com]、ダウンロードパスワード[abcd1234]、
'*    受取人�@名前[花子]、受取人�@メールアドレス[hanako@exsample.com]、メッセージは決裁をお願いいたします.
'*    フラッシュは未対応版
'*
'*    [設置したパス]\DECO_Auto_Complete.vbs -iit -a太郎 -btaro -pabcd1234 -x花子 -yhanako@exsample.com -m決裁をお願いいたします. -f1
'*
'*+ 部署id一覧
'*   kei	経理課					http://fileex.matsusaka.co.jp/keiri/
'*   kik	経営企画部				http://fileex.matsusaka.co.jp/keieikikaku/
'*   kan	管理本部				http://fileex.matsusaka.co.jp/kanri/
'*   eig	営業本部				http://fileex.matsusaka.co.jp/eigyo/
'*   kok	公共システム本部		http://fileex.matsusaka.co.jp/kokyo/
'*   joh	情報システム本部		http://fileex.matsusaka.co.jp/joho/
'*   osc	アウトソーシング本部	http://fileex.matsusaka.co.jp/outsourcing/
'*   nss	名張総合サービス		http://fileex.matsusaka.co.jp/nabarisogo/
'*   it		ITインフラ部			http://fileex.matsusaka.co.jp/it/
'*
'*********************************************************************************
'*
'* 以降、変更箇所なし
'*
'*********************************************************************************
'定数
	const url = "http://fileex.matsusaka.co.jp/"
	const noflashpara ="file_send/index_noflash"
	Const strIEexe = "iexplore.exe" 'IEのプロセス名
	Dim IE  						'IE用変数
	Set IE = CreateObject("InternetExplorer.Application") ' IE起動
	dim qstr
'変数
	Dim intProcID
'メイン
	call main()
'*********************************************************************************
'メイン
'*********************************************************************************
private sub main()
	dim msg
	dim urlpara
	dim busyoid
	dim fromName
	dim fromMail
	dim pwd
	dim toName
	dim toMail
	dim i
	dim iCount
	dim strArg
	dim noFlash

	'引数を調査
	iCount = Wscript.Arguments.Count
	if iCount > 0 then
		for i = 0 to iCount - 1
			strArg = WScript.Arguments(i)
			if instr(strArg,"-i") > 0 then
				busyoid = replace(strArg,"-i","")
			elseif instr(strArg,"-a") > 0 then
				fromName = replace(strArg,"-a","")
			elseif instr(strArg,"-b") > 0 then
				fromMail = replace(strArg,"-b","")
			elseif instr(strArg,"-p") > 0 then
				pwd = replace(strArg,"-p","")
			elseif instr(strArg,"-x") > 0 then
				toName = replace(strArg,"-x","")
			elseif instr(strArg,"-y") > 0 then
				toMail = replace(strArg,"-y","")
			elseif instr(strArg,"-m") > 0 then
				msg = replace(strArg,"-m","")
			elseif instr(strArg,"-f") > 0 then
				noFlash = replace(strArg,"-f","")
			end if
		next
	else
	''引数なしはさすがにエラー
	'	msgbox "引数の指定がありません.",vbCritical,"引数なしエラー"
	'	Wscript.quit
	'引数なしの場合はTOPを開く
	end if
	
	'部署IDの設定がない場合は開くURLがないのでエラー
	select case busyoid
		case "it"
			qstr = "it/"
		case "kei"
			qstr = "keiri/"
		case "kik"
			qstr = "keieikikaku/"
		case "kan"
			qstr = "kanri/"
		case "eig"
			qstr = "eigyo/"
		case "kok"
			qstr = "kokyo/"
		case "joh"
			qstr = "joho/"
		case "osc"
			qstr = "outsourcing/"
		case "nss"
			qstr = "nabarisogo/"
		case else
			msgbox "部署別のページを開く場合は，" & string(1,vbcr) & "引数として[-i]に続けて下記コードを指定します." & string(2,vbcr) & _ 
			       " kei	経理課" & string(1,vbcr) & _ 
			       " kik	経営企画部" & string(1,vbcr) & _ 
			       " kan	管理本部" & string(1,vbcr) & _ 
			       " eig	営業本部" & string(1,vbcr) & _ 
			       " kok	公共システム本部" & string(1,vbcr) & _ 
			       " joh	情報システム本部" & string(1,vbcr) & _ 
			       " osc	アウトソーシング本部" & string(1,vbcr) & _ 
			       " nss	名張総合サービス" & string(1,vbcr) & _ 
			       " it	ITインフラ部",vbInformation,"部署ID未入力？"
			'引数なしの場合はTOPを開く Wscript.quit
	end select

	'urlを組み立てる
	if noFlash then
		urlpara = url & qstr & noflashpara
	else
		urlpara = url & qstr
	end if
	'ieで開く
	call use_ie(urlpara,fromName,fromMail,pwd,toName,toMail,msg)
end sub
'*********************************************************************************
' IE用 Subプロシージャ
'*********************************************************************************
Private Sub use_ie(urlpara,fName,fMail,pwd,tName,tMail,msg)
 	dim objForm

    IE.Navigate urlpara
    IE.Visible = True   ' IEの可視化
    
    call ActiveIE
    waitIE				' IEの起動待機

	'ログインが必要かどうか調べる（いい方法がなくて）
	on error resume next
	dim objDoc
	objDoc = IE.document.getElementById("LoginUserID")

 	if err = 0 then
	 	with IE.Document
			'ログイン
			if fName then .getElementById("send_matter_name").Value = fName
			if fMail then .getElementById("send_matter_mail_address").Value = fMail
			if pwd   then .getElementById("send_matter_receive_password").Value = pwd
			if tName then .getElementById("receiver_1_name").Value = tName
			if tMail then .getElementById("receiver_1_mail_address").Value = tMail
			if msg   then .getElementById("send_matter_message").Value = msg
			WScript.Sleep 500
		end with
	else
		err.clear
	end if
End Sub
'********************************************************************************* 
' IEがビジー状態の間待ちます
'*********************************************************************************
Private Sub waitIE()
    Do While IE.Busy = True Or IE.readystate <> 4
        WScript.Sleep 1000
    Loop
    exit sub
End Sub
'*********************************************************************************
'ウインドウをアクティブにする
'*********************************************************************************
Private sub ActiveIE()
    Dim objWshShell
    GetProcID(strIEexe)
    Set objWshShell = CreateObject("Wscript.Shell")
    objWshShell.AppActivate intProcID
    Set objWshShell = Nothing
End Sub
Private Function GetProcID(ProcessName)
    Dim Service
    Dim QfeSet
    Dim Qfe
    Set Service = WScript.CreateObject("WbemScripting.SWbemLocator").ConnectServer
    Set QfeSet = Service.ExecQuery("Select * From Win32_Process Where Caption='"& ProcessName &"'")
    intProcID = 0
    For Each Qfe in QfeSet
        intProcID = Qfe.ProcessId
        Exit For
    Next
    GetProcID = intProcID <> 0
End Function
