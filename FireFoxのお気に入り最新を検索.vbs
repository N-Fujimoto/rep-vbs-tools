Option Explicit
'=========================================================================================
'	仮想サーバにあるFirefoxのお気に入りフォルダを開く
'	Ver1.0
'	2020/12/18  N-FUJIMOTO 
'=========================================================================================

'設定開始

'↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
''ドメインの管理者
'	const pUS = "matsusaka\100553"
''上記のユーザのパスワード
'	const pPW = "59length"
'表示されるIEのサイズ・位置
	const ieHeight = 380
	const ieWidth = 870
	const ieTop = 0
	const ieLeft = 0
'初期表示したいユーザID	
	const g_strUserID = "100xxx"
'↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


'設定終わり
'Firefoxのユーザファイル
const olpath = "\[userid]\AppData\Roaming\Mozilla\Firefox\Profiles"
const bmpath = "bookmarkbackups"
Const sct = 0 '仮想サーバの数 - 1
ReDim arryPath(sct)
'仮想マシン + users$
arryPath(0) = "\\omecsvm00249\users$"
'=========================================================================================
call main
'=========================================================================================
Private sub main()
'変数
	Dim i, ict, tct
	Dim fs, fl, fi, nt
	Dim strPath, strUserID, strDate, strMSG, strPW
'初期化
	strDate = 0
	ict = 0
	tct = 0
'開始
	Set fs = WScript.CreateObject("Scripting.FileSystemObject")
	Set nt = WScript.CreateObject("WScript.Network")
'エラーは無視
On Error Resume Next
''管理者情報を入力させる
'	if pPW = vbNullString then
'		strPW = InputBox("[" & pUS & "]のパスワードを入力してください.", "パスワードを入力.", pPW)
'		if strPW = vbNullString then WScript.Quit
'	else
'		strPW = pPW
'	end if
'NET USE
	for i = 0 to sct
		'念のため削除
		nt.RemoveNetworkDrive arryPath(i), True
		err.Clear
'		'接続
'		nt.MapNetworkDrive "", arryPath(i), False, pUS, strPW
'		if err <> 0 then
'			MsgBox "下記ユーザはログインできませんでした." & string(2,vbcrlf) & "[" & pUS & "]",vbCritical,"パスワード不整合"
'			wscript.Quit
'		end if
'		err.Clear
	next
'職員番号を入力させる
	strUserID = InputBox("職員番号を入力してください." & string(2,vbCrlf) & "ex)100xxx", "職員番号を入力.", g_strUserID)
	if strUserID = vbNullString then wscript.Quit
	
'フォルダループとファイル検索
	For i = 0 To UBound(arryPath)
	    strPath = Replace(arryPath(i) & olpath, "[userid]", strUserID)
	    Set fl = fs.getFolder(strPath)

	    If Err = 0 Then
	        For Each fi In fl.subfolders
	        	if fs.FolderExists(fs.buildpath(fi.path,bmpath)) then
	        
		        	ReDim Preserve arryTable(5,ict) '2次元配列は最後尾の添え字しか増やせない
		        	
	                arryTable(1,ict) = strPath 'パス
	                arryTable(2,ict) = Left(fi.Path, 14) 'サーバ名
	                arryTable(3,ict) = fs.buildpath(fi.path,bmpath)
	                arryTable(4,ict) = fi.Size
	                arryTable(5,ict) = fi.DateLastModified '更新日
	                
					'更新日が大きいもの
	                If fi.DateLastModified > strDate Then
	                    strDate = fi.DateLastModified
	                    tct = ict
	                End If
	                ict = ict + 1
                end if
	        Next
	    Else
	        Err.Clear
	    End If
	    Set fl = Nothing
	Next
'NET USE 切断
	for i = 0 to sct
		nt.RemoveNetworkDrive arryPath(i), True
		err.Clear
	next
'検索結果メッセージ整形
	strMSG = "<table border=1 cellpadding=2 cellspacing=0><tr>" & _
			 "<td>最新</td><td>仮想サーバ</td><td>Firefoxお気に入り</td><td>バイト</td><td>更新日</td></tr>"
	For i = 0 To ict - 1
	    If Len(arryTable(1,i)) > 0 Then
	        If i = tct Then
	            arryTable(0,i) = "＊"
	        End If
	        strMSG = strMSG & _
	        		 "<tr><td>" & arryTable(0,i) & "</td>" & _
	        		 "<td><a href='" & arryTable(3,i) & "'>" & arryTable(2,i) & "</a></td>" & _
	        		 "<td>" & arryTable(3,i) & "</td>" & _
	        		 "<td>" & arryTable(4,i) & "</td>" & _
	        		 "<td>" & arryTable(5,i) & "</td></tr>"
	    End If
	Next
	strMSG = strMSG & "</table>"
'検索結果
	If Len(arryTable(1,0)) = 0 Then strMSG = "[" & strUserID & "]の対象のファイルはありませんでした."
	strMSG = strMSG & _
			 "<br><div><input type='button' value='ウィンドウを閉じる' onClick='window.close();'></div>"
	'test MsgBox strMSG, , "Firefoxファイル検索結果"
	call IEMessage(strMSG)
'破棄
	Set fs = Nothing
	set nt = nothing

end sub
'=========================================================================================
'	IEでメッセージ表示する
'=========================================================================================
private sub IEMessage(byval strMSG)
	Dim IE
	Set IE = WScript.CreateObject("InternetExplorer.Application")
	
	IE.Navigate "about:_self"
	While IE.busy: Wend
	While IE.Document.readyState <> "complete": DoEvents : Wend
	IE.Document.body.innerHTML = strMSG
	IE.AddressBar = False
	IE.ToolBar = False
	IE.StatusBar = False
	IE.Height = ieHeight
	IE.Width = ieWidth
	IE.Visible = True
	IE.Top = ieTop
	IE.Left = ieLeft

end sub