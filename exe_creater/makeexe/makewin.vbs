option explicit
const mke = "C:\VBS tools\exe_creater\makeexe\makewin.exe"  'makewin.exeのパス


dim param, sfo, wss
dim fpath, dpath, fname, epath, exepath
set param = wscript.arguments
if param.count = 0 then
	fpath = inputbox("ファイルのパスを入力")
	if isempty(fpath) then wscript.quit
	if fpath = vbNullString then wscript.quit
else
    fpath = param(0)
end if
set sfo = createobject("scripting.filesystemobject")
set wss = createobject("wscript.shell")
dpath = sfo.GetParentFolderName(fpath)
fname = sfo.getBaseName(fpath)

exepath = sfo.buildpath(dpath, fname & ".exe")
set sfo = createobject("scripting.filesystemobject")
set wss = createobject("wscript.shell")
'exe delete
if sfo.FileExists(exepath) then Call sfo.deletefile(exepath)
call wss.exec(mke & " """ & fpath & """")

set param = nothing
set sfo = nothing
set wss = nothing
