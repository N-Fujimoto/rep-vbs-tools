'The LGPL license is not applired to following code.
'It is free to use for any purpose without any gurantee.

WScript.echo(ModulePath)

Function ModulePath()
  dim T,fHandle
  ModulePath=WScript.ScriptFullName
  T=lcase(ModulePath)
  T=left(T,len(T)-4)
  if right(T,4)<>".tmp" then exit function
  on error resume next
  set fHandle=CreateObject("Scripting.FileSystemObject").OpenTextFile(T)
  T=fHandle.ReadLine
  fHandle.close
  if err.number=0 then ModulePath=T
End Function