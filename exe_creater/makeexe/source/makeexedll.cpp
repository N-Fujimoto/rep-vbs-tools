#include <windows.h>
#pragma comment(linker, "/nodefaultlib:\"libc.lib\"")
#pragma comment(linker, "/noentry")

int EndWithError(LPSTR message){
	MessageBox(0,message,"Error",16);
	return 1;
}

char ReadChar(HANDLE fHandle){
	UINT fSize;
	char buff;
	ReadFile(fHandle,&buff,1,(DWORD*)&fSize,NULL);
	if (!fSize) return 0;
	return buff;
}
BOOL WriteChar(HANDLE fHandle2, char buff){
	UINT fSize;
	WriteFile(fHandle2,&buff,1,(DWORD*)&fSize,NULL);
	if (!fSize) return FALSE;
	return TRUE;
}

STDAPI MakeExe( HINSTANCE hInst, HINSTANCE hPrevInst, 
                    LPSTR lpCmdLine, int intShowCmd)
{
	HANDLE fHandle,fHandle2,fHandle3;
	UINT fSize,fSize2;
	int i,j;
	char c=0;
	char *ANSI;
	char* VBSorJS;
	char szModulePath[MAX_PATH];
	char buff[1024];
	BOOL IsWSF=FALSE;
	const LPSTR CWTEF="Cannot write to exe file!";

	GetModuleFileName(hInst,szModulePath,MAX_PATH);

	//Create new EXE
	if (!lpCmdLine) return EndWithError("Please drag & drop a script file.");
	char exeName[MAX_PATH];
	char scriptName[MAX_PATH];
	if (lpCmdLine[0]=='\"') for (i=0;(scriptName[i]=lpCmdLine[i+1])!='\"';i++);
	else for (i=0;scriptName[i]=lpCmdLine[i];i++);
	for (scriptName[i]=0;scriptName[i]!='.';i--);
	lstrcpy(exeName,scriptName);
	ANSI=(char*)((int)scriptName+i);//extension
	if      (lstrcmpi(ANSI,".vbs")==0) VBSorJS="'*****Begin VBS*****";
	else if (lstrcmpi(ANSI,".vbe")==0) VBSorJS="'*****Begin VBE*****";
	else if (lstrcmpi(ANSI,".js")==0)  VBSorJS="//*****Begin JS*****";
	else if (lstrcmpi(ANSI,".jse")==0) VBSorJS="//*****Begin JSE****";
	else if (lstrcmpi(ANSI,".wsf")==0) {
		VBSorJS="<!--**Begin WSF**-->";
		IsWSF=TRUE;
	} else return EndWithError("Not a script file");
	for (j=0;exeName[++i]="exe"[j];j++);

	fHandle2=CreateFile(exeName,GENERIC_WRITE,0,NULL,CREATE_NEW,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	if (fHandle2==INVALID_HANDLE_VALUE) return EndWithError("Cannot open exe file for writing");
	fHandle=CreateFile(szModulePath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	if (fHandle==INVALID_HANDLE_VALUE) return EndWithError("Cannot open myself");

	//Copy myself
	while (TRUE) {
		ReadFile(fHandle,buff,1024,(DWORD*)&fSize,NULL);
		if (fSize==0) break;
		WriteFile(fHandle2,buff,fSize,(DWORD*)&fSize,NULL);
		if (fSize<1024) break;
	}
	CloseHandle(fHandle);

	//Write separator
	WriteFile(fHandle2,VBSorJS,lstrlen(VBSorJS),(DWORD*)&fSize,NULL);
	WriteFile(fHandle2,"\x0D\x0A",2,(DWORD*)&fSize,NULL);

	//Copy script file
	fHandle=CreateFile(scriptName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	if (fHandle2==INVALID_HANDLE_VALUE) return EndWithError("Cannot open script file");
	if (!IsWSF) {
		while (TRUE) {
			ReadFile(fHandle,buff,1024,(DWORD*)&fSize,NULL);
			if (fSize==0) break;
			fSize2=fSize;
			WriteFile(fHandle2,buff,fSize,(DWORD*)&fSize,NULL);
			if (fSize<fSize2) return EndWithError(CWTEF);
			if (fSize<1024) break;
		}
	} else {//WSF file
		BOOL isXML=FALSE;
		BOOL inTag=FALSE;
		BOOL inScript=FALSE;
		int inRem=0;//1: in Rem, 2: first '-', 3: 2nd '-'
		while(TRUE) {
			if (!(c=ReadChar(fHandle))) break;
			if (inRem) {
				switch (inRem) {
				case 2:
					if (c=='-') inRem=3;
					else inRem=1;
					break;
				case 3:
					if (c=='>') inRem=0;
					else inRem=1;
					break;
				default:
					if (c=='-') inRem=2;
				}
			} else if (!inTag) {
				if (c=='<') inTag=TRUE;
			} else {//inTag
				if (c=='!') {//maybe remark
					inTag=FALSE;
					if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
					if (!(c=ReadChar(fHandle))) break;
					if (c=='>') {//not remark
						if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						continue;
					} else if (c!='-') {//not remark
						if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						while (c!='>') {
							if (!(c=ReadChar(fHandle))) break;
							if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						}
						continue;
					}
					if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
					if (!(c=ReadChar(fHandle))) break;
					if (c!='-') {//not remark
						if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						while (c!='>') {
							if (!(c=ReadChar(fHandle))) break;
							if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						}
						continue;
					} else {//remark
						if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						inRem=1;
						continue;
					}
				}
				//inTag
				i=0;
				while (c==0x20 || c==0x9) if (!(c=ReadChar(fHandle))) break;
				while (TRUE) {
					if (c=='>') c=0;
					if (c==0x9) c=0x20;
					buff[i++]=c;
					if (c==0) break;
					if (!(c=ReadChar(fHandle))) break;
				}
				while (0<i) {
					if (buff[i-1]!=0x20 && buff[i-1]!=0x9) break;
					buff[--i]=0;
				}
				inTag=FALSE;
				if (lstrcmpi(buff,"?xml")>=0 && lstrcmpi(buff,"?xml!")<=0) {
					isXML=TRUE;
				} else if (lstrcmpi(buff,"/script")>=0 && lstrcmpi(buff,"/script!")<=0) {
					inScript=FALSE;
				} else if (!inScript) {
					//<script ...
					if (lstrcmpi(buff,"script")>=0 && lstrcmpi(buff,"script!")<=0) {
						inScript=TRUE;
						LPSTR src=0;
						BOOL endTag=FALSE;
						for (i=0;i<7;i++) if (!WriteChar(fHandle2,buff[i])) return EndWithError(CWTEF);
						while (TRUE){
							while (buff[i]==0x20) i++;
							if (lstrcmpi(buff+i,"src ")>=0 && lstrcmpi(buff+i,"src!")<=0 ||
									lstrcmpi(buff+i,"src= ")>=0 && lstrcmpi(buff+i,"src=\\")<=0) {
								// src="..."
								src=(char*)&szModulePath;
								while (buff[i++]!='=');
								while (buff[i]==0x20) i++;
								switch ((c=buff[i])){
								case '\"':
								case '\'':
									i++;
									for (j=0;(src[j]=buff[i++])!=c;j++);
									break;
								default:
									for (j=0;(src[j]=buff[i++])!=0x20;j++);
								}
								src[j]=0;
							} else if (buff[i]=='/') {
								endTag=TRUE;
								break;
							} else if (!buff[i]) {
								break;
							} else {
								// xxx="..."
								j=i;
								while (buff[i++]!='=');
								while (buff[i]==0x20) i++;
								switch ((c=buff[i])){
								case '\"':
								case '\'':
									i++;
									while (buff[i++]!=c);
									break;
								default:
									while (buff[i]!=0x20) i++;
								}
								while (j<=i && buff[j]) if (!WriteChar(fHandle2,buff[j++])) return EndWithError(CWTEF);
								if (!WriteChar(fHandle2,0x20)) return EndWithError(CWTEF);
							}
						}
						if (!WriteChar(fHandle2,'>')) return EndWithError(CWTEF);
						if (src) {
							// pre script
							for (i=0;c="\x0D\x0A"[i];i++) if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
							if (isXML) for (i=0;c="<![CDATA[\x0D\x0A"[i];i++) if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
							//resolve file name
							if (src[0]=='\\' || src[1]==':') {
								for (i=0;buff[i]=src[i];i++) if (src[i]=='/') buff[i]='\\';
							} else {
								j=0;
								for (i=0;buff[i]=scriptName[i];i++) if (scriptName[i]=='\\') j=i+1;
								i=j;
								for (j=0;buff[i++]=src[j];j++) if (src[j]=='/') buff[i-1]='\\';
							}
							fHandle3=CreateFile(buff,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
							if (fHandle3==INVALID_HANDLE_VALUE) {
								for (i=0;c="Cannot open \""[i];i++) buff[i]=c;
								for (j=0;buff[i]=src[j++];i++);
								for (j=0;buff[i]="\"!"[j++];i++);
								return EndWithError(buff);
							}
							// copy the script from file
							while (TRUE) {
								ReadFile(fHandle3,buff,1024,(DWORD*)&fSize,NULL);
								if (fSize==0) break;
								fSize2=fSize;
								WriteFile(fHandle2,buff,fSize,(DWORD*)&fSize,NULL);
								if (fSize<fSize2) return EndWithError(CWTEF);
								if (fSize<1024) break;
							}
							CloseHandle(fHandle3);
							// post script
							if (isXML) for (i=0;c="\x0D\x0A]]>"[i];i++) if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
							for (i=0;c="\x0D\x0A"[i];i++) if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
							if (endTag) for (i=0;c="</script>\x0D\x0A"[i];i++) if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
						}
						continue;
					}
				}
				for (i=0;c=buff[i];i++) if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
				c='>';
			}
			if (!WriteChar(fHandle2,c)) return EndWithError(CWTEF);
		}
	}

	CloseHandle(fHandle);
	CloseHandle(fHandle2);
	MessageBox(0,"EXE file is succesfully made.","MakeExe",0);//*/
	return 0;
}

