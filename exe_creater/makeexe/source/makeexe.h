#include <windows.h>
#pragma comment(linker, "/nodefaultlib:\"libc.lib\"")
#pragma comment(linker, "/entry:\"PreWinMain\"")

int WINAPI WinMain( HINSTANCE hInst, HINSTANCE hPrevInst, 
                    LPSTR lpCmdLine, int intShowCmd);
int PreWinMain();
BOOL CALLBACK EnumWindowsProc(HWND hwnd,DWORD dwThreadId);
void DoEvents();
void zMemory(void* pointer,int size);
int EndWithError(LPSTR message);
