#include "makeexe.h"

int WINAPI WinMain( HINSTANCE hInst, HINSTANCE hPrevInst, 
                    LPSTR lpCmdLine, int intShowCmd)
{
	HANDLE fHandle,fHandle2;
	UINT fSize;
	int i,j;
	char c=0;
	char *ANSI;
	char VBS[MAX_PATH],TMP[MAX_PATH],TMPFILE[MAX_PATH];
	char* VBSorJS;
	char szModulePath[MAX_PATH];
	char buff[1024];
	BOOL CONSOLE=FALSE;
	if (GetConsoleTitle(TMP,MAX_PATH)) CONSOLE=TRUE;
	GetModuleFileName(hInst,szModulePath,MAX_PATH);
	fHandle=CreateFile(szModulePath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
	if (fHandle==INVALID_HANDLE_VALUE) return EndWithError("Cannot open myself");

	//The script starts with "'*****Begin VBS*****" etc.
	ANSI="*****Begin VBS*****\x0D\x0A";

	while (TRUE) {
		if (!ReadFile(fHandle,&c,1,(DWORD*)&fSize,NULL)) break;
		if (fSize==0) break;
		if (c=='\''|| c=='/' || c=='<') {
			ReadFile(fHandle,ANSI,21,(DWORD*)&fSize,NULL);
			if      (lstrcmp(ANSI,"*****Begin VBS*****\x0D\x0A")==0 && fSize==21) VBSorJS=".vbs";
			else if (lstrcmp(ANSI,"*****Begin VBE*****\x0D\x0A")==0 && fSize==21) VBSorJS=".vbe";
			else if (lstrcmp(ANSI,"/*****Begin JS*****\x0D\x0A")==0 && fSize==21) VBSorJS=".js";
			else if (lstrcmp(ANSI,"/*****Begin JSE****\x0D\x0A")==0 && fSize==21) VBSorJS=".jse";
			else if (lstrcmp(ANSI,"!--**Begin WSF**-->\x0D\x0A")==0 && fSize==21) VBSorJS=".wsf";
			else VBSorJS=0;
			if (VBSorJS){
				//Make temporary file (*.vbs) and open it for writing.
				TMPFILE[0]=0;
				GetTempPath(MAX_PATH,TMP);
				do {
					if (TMPFILE[0]) DeleteFile(TMPFILE);//second round.
					if (!GetTempFileName(TMP,"SFC",0,TMPFILE)) return 1;
					//write ModuleFileName
					fHandle2=CreateFile(TMPFILE,GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
					if (fHandle2!=INVALID_HANDLE_VALUE) {
						fSize=lstrlen(szModulePath);
						WriteFile(fHandle2,szModulePath,fSize,(DWORD*)&fSize,NULL);
						CloseHandle(fHandle2);
					}
					for (i=0;VBS[i]=TMPFILE[i];i++);
					for (j=0;(VBS[i++]=VBSorJS[j])!=0;j++);
					fHandle2=CreateFile(VBS,GENERIC_WRITE,0,NULL,CREATE_NEW,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
				} while (fHandle2==INVALID_HANDLE_VALUE);

				//copy the script from exe module to vbs.
				while (TRUE) {
					ReadFile(fHandle,buff,1024,(DWORD*)&fSize,NULL);
					if (fSize==0) break;
					WriteFile(fHandle2,buff,fSize,(DWORD*)&fSize,NULL);
					if (fSize<1024) break;
				}
				CloseHandle(fHandle2);
				CloseHandle(fHandle);

				//prepare for CreateProcess
				if (CONSOLE) for (i=0;(buff[i]="cscript.exe //Nologo \""[i])!=0;i++);
				else for (i=0;(buff[i]="wscript.exe \""[i])!=0;i++);
				for (j=0;(buff[i]=VBS[j++])!=0;i++);
				for (j=0;(buff[i]="\" "[j++])!=0;i++);
				if (lpCmdLine) for (j=0;(buff[i++]=lpCmdLine[j])!=0;j++);
				else buff[i-1]=0;
				STARTUPINFO si;
				PROCESS_INFORMATION pi;
				zMemory(&si, sizeof(si));
				zMemory(&pi, sizeof(pi));
				si.cb = sizeof(si);
				si.dwFlags=STARTF_USESHOWWINDOW;
				si.wShowWindow=intShowCmd;

				//Start Process
				if (CreateProcess(NULL, buff, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {

					while (WaitForSingleObject(pi.hProcess,0)!=WAIT_TIMEOUT) DoEvents;
					while (EnumWindows((WNDENUMPROC)*EnumWindowsProc,pi.dwThreadId)) {
						DoEvents;
						if (WaitForSingleObject(pi.hProcess,0)!=WAIT_TIMEOUT) break;//check if process is still acvite.
					}

					WaitForSingleObject(pi.hProcess,INFINITE);//wait until end of process
					GetExitCodeProcess(pi.hProcess,(LPDWORD)&i);
					CloseHandle(pi.hThread);
					CloseHandle(pi.hProcess);
					DeleteFile(VBS);
					DeleteFile(TMPFILE);
					return i;
				} else EndWithError("Cannot start process");
			}
		}
	}
	CloseHandle(fHandle);

	HMODULE hDll=LoadLibrary("makeexe.dll");
	if (!hDll) return EndWithError("Loading makeexe.dll failed");
	FARPROC MakeExe=GetProcAddress(hDll,"MakeExe");
	if (!MakeExe) return EndWithError("MakeExe not found in makeexe.dll.");
	_asm push intShowCmd;
	_asm push lpCmdLine;
	_asm push hPrevInst;
	_asm push hInst;
	_asm call MakeExe;
	_asm mov i,eax;
	return i;
}
int PreWinMain(){
	LPSTR lpCmdLine=GetCommandLine();
	STARTUPINFO si;
	GetStartupInfo(&si);
	if (lpCmdLine) {
		UINT i;
		if (lpCmdLine[0]=='\"') for(i=1;lpCmdLine[i++]!='\"';);
		else for(i=0;lpCmdLine[i]!=' ' && lpCmdLine[i]!=0;i++);
		if (lpCmdLine[i]==0 || lpCmdLine[i+1]==0) lpCmdLine=0;
		else lpCmdLine=(LPSTR)((UINT)lpCmdLine+i+1);
	}
	return WinMain(GetModuleHandle(NULL),NULL,lpCmdLine,si.wShowWindow);
}
BOOL CALLBACK EnumWindowsProc(HWND hwnd,DWORD dwThreadId){
    if (GetWindowThreadProcessId(hwnd,NULL)==dwThreadId) {
        if (SetForegroundWindow(hwnd)) return FALSE;
    }
    return TRUE;
}

void DoEvents(){
    MSG msg;
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}
void zMemory(void* pointer,int size){
	char* pt=(char*)pointer;
	for (int i=0;i<size;i++) pt[i]=0;
}

int EndWithError(LPSTR message){
	MessageBox(0,message,"Error",0);
	return 1;
}