/*

  The LGPL license is not applired to following code.
  It is free to use for any purpose without any gurantee.

*/

WScript.Echo(ModulePath());

function ModulePath(){
  var ret=WScript.ScriptFullName;
  var T=ret.toLowerCase();
  T=T.substring(0,T.length-3);
  if (T.charAt(T.length-1)=='.') T=T.substring(0,T.length-1);
  if (T.substring(T.length-4,T.length)!='.tmp') return WScript.ScriptFullName;
  try {
    var fHandle=WScript.CreateObject("Scripting.FileSystemObject").OpenTextFile(T);
    T=fHandle.ReadLine();
    fHandle.Close();
    ret=T;
  } finally {
    return ret;
  }
}