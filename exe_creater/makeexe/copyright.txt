Copyright (C) 2010年、Katsumi

この文書は、makeexe.dll、makewin.exe、makeconsole.exeの３つの実行ファ
イルと、それらのソースコードに関するものです。

このライブラリはフリーソフトウェアです。あなたはこれを、フリーソフトウェ
ア財団によって発行されたGNU 劣等一般公衆利用許諾契約書(バージョン2.1 
か、希望によってはそれ以降のバージョンのうちどれか)の定める条件の下で
再頒布または改変することができます。

このライブラリは有用であることを願って頒布されますが、*全くの無保証* 
です。商業可能性の保証や特定の目的への適合性は、言外に示されたものも含
め全く存在しません。詳しくはGNU 劣等一般公衆利用許諾契約書をご覧くださ
い。

あなたはこのライブラリと共に、GNU 劣等一般公衆利用許諾契約書の複製物を
一部受け取ったはずです。もし受け取っていなければ、フリーソフトウェア財
団まで請求してください(宛先は the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA)。

加えて、特別な例外として、このソフトウェアの正規な使用により作成された
EXEファイルに付いては、GNU 劣等一般公衆利用許諾契約書を適用しなくても
良いものとします。あなたがこのライブラリを改変したならば、あなたはこ
の例外をあなたのバージョンのライブラリに引き続き設けることもできます
が、そうする義務はありません。もし例外を設けたくなければ、この例外条項
をあなたのバージョンからは削除してください。

Copyright (C) 2010  Katsumi

This document is written for the makeexe.dll, makewin.exe, makeconsole.exe,
and their source codes.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

In addition, as a special exception, the EXE files that are made by
this software by the normal-use does not have to be the GNU Lesser 
General Public License libraries. If you modify this library, you may 
extend this exception to your version of the library, but you are not 
obligated to do so.  If you do not wish to do so, delete this exception 
statement from your version.

