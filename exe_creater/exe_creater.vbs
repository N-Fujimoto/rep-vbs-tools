option explicit
const sen = "C:\VBS tools\exe_creater\screnc.exe"    'screnc.exeのパス
const mke = "C:\VBS tools\exe_creater\makeexe\makewin.exe"  'makewin.exeのパス

dim param, cname, sfo, wss
dim fpath, dpath, ftype, fname, epath, exepath
set param = wscript.arguments
if param.count = 0 then
	fpath = inputbox("ファイルのパスを入力")
	if isempty(fpath) then wscript.quit
	if fpath = vbNullString then wscript.quit
else
    fpath = param(0)
end if
set sfo = createobject("scripting.filesystemobject")
set wss = createobject("wscript.shell")
dpath = sfo.GetParentFolderName(fpath)
ftype = sfo.getExtensionName(fpath)
fname = sfo.getBaseName(fpath)
if lcase(ftype) = "hta" then
    epath = sfo.buildpath(dpath, fname & ".html")
    set cname = sfo.getfile(fpath)
    cname.name = fname & ".html"
    wss.run sen & " """ & epath & """ """ & fpath & """", 0, true
    msgbox "完了",, "確認"
    wscript.quit
end if
if lcase(ftype) = "vbs" then
    epath = sfo.buildpath(dpath, fname & ".vbe")
elseif lcase(ftype) = "js" then
    epath = sfo.buildpath(dpath, fname & ".jse")
elseif lcase(ftype) = "wsf" then
    epath = fpath
    set cname = sfo.getfile(fpath)
    cname.name = fname & ".html"
    fpath = sfo.buildpath(dpath, fname & ".html")
end if
exepath = sfo.buildpath(dpath, fname & ".exe")
'vbs => vbe
call wss.run("""" & sen & """ """ & fpath & """ """ & epath & """", 0, true)
'exe delete
if sfo.FileExists(exepath) then Call sfo.deletefile(exepath)
'vbe => exe
call wss.exec(mke & " """ & epath & """")
'vbe delegte
call sfo.deletefile(epath)

set param = nothing
set cname = nothing
set sfo = nothing
set wss = nothing
