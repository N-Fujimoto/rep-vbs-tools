echo off
rem ----------------------------------------------------------------------------
rem 設定
rem ----------------------------------------------------------------------------
rem 繰り返し回数
set times=20
rem swipe速度
set intr=500
rem ----------------------------------------------------------------------------

rem ----------------------------------------------------------------------------
rem 処理開始
rem ----------------------------------------------------------------------------
setlocal EnableDelayedExpansion
cd c:\adb
adb devices
echo !date! !time! 回数(%times%) 開始しました...
for /l %%m in (1,1,%times%) do (
	echo !date! !time! %%m times start
	for /l %%n in (800,100,1200) do (
		set /a ran1=!random!*11/32767
		set /a ran2=!random!*11/32767
		set /a ran3=!random!*51/32767
		set /a tp1=%%n+!ran1!
		set /a tp2=1060+!ran2!
		echo !date! !time! %%m times swipe ^(x:!ran3!,y:!tp1!^) ^(x:!tp2!,y:!tp1!^)
		adb.exe shell input swipe !ran3! !tp1! !tp2! !tp1! %intr%
		timeout /t 1 > nul
	)
	set /a ran1=!random!*11/32767
	set /a ran2=!random!*11/32767
	set /a ran3=!random!*2/32767+1
	set /a ran4=!random!*2/32767+2
	set /a xp=960+!ran1!
	set /a yp=530+!ran2!
	echo !date! !time! %%m times tap1 and wait !ran3!	
	adb.exe shell input tap !xp! !yp!
	timeout /t !ran3! > nul 
	set /a xp=780+!ran1!
	set /a yp=960+!ran2!
	echo !date! !time! %%m times tap2 and wait !ran4!	
	adb.exe shell input tap !xp! !yp!
	timeout /t !ran4! > nul
	echo !date! !time! %%m times end
)
echo !date! !time! 回数(%times%) 終了しました.
pause
rem ----------------------------------------------------------------------------
goto :eof
