Option Explicit
'=========================================================================================
'	仮想サーバにあるOutlookファイルを検索して更新日の新しいものを調査する
'	Ver1.2
'	2017/11/01  N-FUJIMOTO 
'=========================================================================================

'設定開始

'↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
'ドメインの管理者
	const pUS = "matsusaka\administrator"
'上記のユーザのパスワード
	const pPW = "fqqm2qpk"
'表示されるIEのサイズ・位置
	const ieHeight = 380
	const ieWidth = 780
	const ieTop = 0
	const ieLeft = 0
'初期表示したいユーザID	
	const g_strUserID = "100xxx"
'↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


'設定終わり
'Outlookのユーザファイル
const olpath = "\Users\[userid]\AppData\Local\Microsoft\Outlook\"
Const sct = 7 '仮想サーバの数
ReDim arryPath(sct)
'仮想マシン + c$
arryPath(0) = "\\OMECVSV00240\c$"
arryPath(1) = "\\OMECVSV00241\c$"
arryPath(2) = "\\OMECVSV00242\c$"
arryPath(3) = "\\OMECVSV00243\c$"
arryPath(4) = "\\OMECVSV00244\c$"
arryPath(5) = "\\OMECVSV00246\c$"
arryPath(6) = "\\OMECVSV00247\c$"
arryPath(7) = "\\OMECVSV00248\c$"
'=========================================================================================
call main
'=========================================================================================
Private sub main()
'変数
	Dim i, ict, tct
	Dim fs, fn, fl, fi, nt
	Dim strPath, strUserID, strDate, strMSG, strPW
'初期化
	ReDim arryTable(sct, 5)
	strDate = 0
	ict = 0
	tct = 0
'開始
	Set fs = WScript.CreateObject("Scripting.FileSystemObject")
	Set nt = WScript.CreateObject("WScript.Network")
'エラーは無視
On Error Resume Next
'管理者情報を入力させる
	if pPW = vbNullString then
		strPW = InputBox("[" & pUS & "]のパスワードを入力してください.", "パスワードを入力.", pPW)
		if strPW = vbNullString then WScript.Quit
	else
		strPW = pPW
	end if
'NET USE
	for i = 0 to sct
		'念のため削除
		nt.RemoveNetworkDrive arryPath(i), True
		err.Clear
		'接続
		nt.MapNetworkDrive "", arryPath(i), False, pUS, strPW
		if err <> 0 then
			MsgBox "下記ユーザはログインできませんでした." & string(2,vbcrlf) & "[" & pUS & "]",vbCritical,"パスワード不整合"
			wscript.Quit
		end if
		err.Clear
		arryPath(i) = arryPath(i) & olpath
	next
'職員番号を入力させる
	strUserID = InputBox("職員番号を入力してください." & string(2,vbCrlf) & "ex)100xxx", "職員番号を入力.", g_strUserID)
	if strUserID = vbNullString then wscript.Quit
'フォルダループとファイル検索
	For i = 0 To UBound(arryPath)
	    strPath = Replace(arryPath(i), "[userid]", strUserID)
	    
	    Set fl = fs.getFolder(strPath)
	    If Err = 0 Then
	        For Each fi In fl.Files
	            If InStr(fi.Name, "@matsusaka.co.jp.pst") > 0 Or _
	               InStr(fi.Name, "@matsusaka.co.jp.ost") > 0 Then
	            
	                Set fn = fs.GetFile(fi.Path)
	                arryTable(ict, 1) = strPath 'パス
	                arryTable(ict, 2) = Left(fi.Path, 14) 'サーバ名
	                arryTable(ict, 3) = fi.Name 'ファイル名
	                arryTable(ict, 4) = fn.Size 'ファイルサイズ
	                arryTable(ict, 5) = fn.DateLastModified '更新日
	
	                '更新日が大きいもの
	                If fn.DateLastModified > strDate Then
	                    strDate = fn.DateLastModified
	                    tct = ict
	                End If
	                
	                ict = ict + 1
	                Set fn = Nothing
	                
	            End If
	        Next
	    Else
	        Err.Clear
	    End If
	    Set fl = Nothing
	Next
'NET USE 切断
	for i = 0 to sct
		arryPath(i) = replace(arryPath(i),olpath,vbNullString)
		nt.RemoveNetworkDrive arryPath(i), True
		err.Clear
	next
'検索結果メッセージ整形
	strMSG = "<table border=1 cellpadding=2 cellspacing=0><tr>" & _
			 "<td>最新</td><td>仮想サーバ</td><td>Outlookファイル</td><td>バイト</td><td>更新日</td></tr><tr>"
	For i = 0 To ict
	    If Len(arryTable(i, 1)) > 0 Then
	        If i = tct Then
	            arryTable(i, 0) = "＊"
	        End If
	        strMSG = strMSG & _
	        		 "<td>" & arryTable(i, 0) & "</td>" & _
	        		 "<td><a href='" & arryTable(i, 1) & "'>" & arryTable(i, 2) & "</a></td>" & _
	        		 "<td>" & arryTable(i, 3) & "</td>" & _
	        		 "<td>" & arryTable(i, 4) & "</td>" & _
	        		 "<td>" & arryTable(i, 5) & "</td></tr>"
	    End If
	Next
	strMSG = strMSG & "</table>"
'検索結果
	If Len(arryTable(0, 1)) = 0 Then strMSG = "[" & strUserID & "]の対象のファイルはありませんでした."
	strMSG = strMSG & _
			 "<br><div><input type='button' value='ウィンドウを閉じる' onClick='window.close();'></div>"
	'MsgBox strMSG, , "Outlookファイル検索結果"
	call IEMessage(strMSG)
'破棄
	Set fs = Nothing
	set nt = nothing

end sub
'=========================================================================================
'	IEでメッセージ表示する
'=========================================================================================
private sub IEMessage(byval strMSG)
	Dim IE
	Set IE = WScript.CreateObject("InternetExplorer.Application")
	
	IE.Navigate "about:_self"
	While IE.busy: Wend
	While IE.Document.readyState <> "complete": DoEvents : Wend
	IE.Document.body.innerHTML = strMSG
	IE.AddressBar = False
	IE.ToolBar = False
	IE.StatusBar = False
	IE.Height = ieHeight
	IE.Width = ieWidth
	IE.Visible = True
	IE.Top = ieTop
	IE.Left = ieLeft
	'WScript.Sleep(3000) '実際は時間の掛かる処理
	'IE.Document.getElementById("msg").innerHTML="完了しました"
	'IE.Quit
	
end sub