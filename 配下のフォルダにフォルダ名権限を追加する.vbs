Option Explicit
'----------------------------------------------------------------------
'配下のフォルダをフォルダ名で所有者を変更する
'2019/10/16  N-FUJIMOTO
'2019/11/22  N-FUJIMOTO テストできるよう修正
'2019/11/26  N-FUJIMOTO 初期値不足でエラーにならないようにトラップ
'----------------------------------------------------------------------
'domain
Const domain = "matsusaka"
'ログファイル名接尾語（接頭語はフォルダ名）
Const icaclslog = "icacls.log"
'ユーザフォルダルート
Const uFolder = "."
'テストユーザ（テストの場合）
Const testUser = ""
'----------------------------------------------------------------------
Dim fso
Dim folder
Dim shl
Dim cmd, cmd1, cmd2, cmd3, cmd4
Dim subfolder
Dim usr
Dim arr
Dim logfile
Dim ret
Dim bolRun '2019/11/26  N-FUJIMOTO add

'2019/11/26  N-FUJIMOTO add s
If (domain = vbNullString Or uFolder = vbNullString) Then
    MsgBox "定数 [ domain ] [ uFolder ] を確認してください.", vbCritical, "初期値エラー"
    WScript.Quit
End If
If Not (testUser = vbNullString) Then
    ret = MsgBox("テストユーザ [ " & testUser & " ]が指定されています.このユーザでテストを行いますか.", vbQuestion + vbYesNo, "テスト確認")
Else
    ret = MsgBox("[" & uFolder & "]配下のフォルダの所有者及び権限を設定します.よろしいですか.", vbQuestion + vbYesNo, "処理確認")
End If
'2019/11/26  N-FUJIMOTO add e

If Not (ret = vbYes) Then WScript.Quit

Set fso = CreateObject("Scripting.FileSystemObject")
Set folder = fso.GetFolder(uFolder)
Set shl = CreateObject("WScript.Shell")

For Each subfolder In folder.subfolders
    arr = Split(subfolder, "\")
    usr = arr(UBound(arr))
    
    'テストの場合 2019/11/26  N-FUJIMOTO add s
    bolRun = False
    If testUser = vbNullString Then
        bolRun = True
    ElseIf usr = testUser Then
        bolRun = True
    End If
    '2019/11/26  N-FUJIMOTO add e

    If bolRun Then '2019/11/26  N-FUJIMOTO add s
    
        logfile = fso.BuildPath(fso.getParentFolderName(WScript.ScriptFullName), usr & "_" & icaclslog)
        'msgbox logfile
        
        cmd = "cmd /c "
        '所有者を変更
        cmd1 = "icacls " & subfolder & " /setowner " & domain & "\" & usr & " /T >> " & logfile & " && "
        '権限を更新
        cmd2 = "icacls " & subfolder & " /grant " & domain & "\" & usr & ":(OI)(CI)(F) >> " & logfile & " && "
        '権限確認
        cmd3 = "icacls " & subfolder & " >> " & logfile & " && "
        '所有者確認
        cmd4 = "dir " & subfolder & " /q >> " & logfile
        'コマンド結合
        cmd = cmd & cmd1 & cmd2 & cmd3 & cmd4
        
        MsgBox cmd
        'shl.Run cmd,1,false
    
        '2019/11/26  N-FUJIMOTO add s
        If Not (testUser = vbNullString) Then
            Exit For
        End If
        '2019/11/26  N-FUJIMOTO add e
    
    End If '2019/11/26  N-FUJIMOTO add e
Next

MsgBox "完了しました.配下のログを確認してください.", vbInformation, "完了"
