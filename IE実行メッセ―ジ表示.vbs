Option Explicit
'****************************************************************************************
' 実行中メッセージをIEのウインドウで表示するツール
' 2019/11/01  N-FUJIMOTO 
'****************************************************************************************

'*** ↓ IE message ******************************************
'変数
Dim result
Dim IE, strMsg, intProcID
Set IE = CreateObject("InternetExplorer.Application")
result = 1 'エラーを発生させるなら↓を[1〜4]に変更する
'*** ↑ IE message ******************************************

Call viewWait(IE, result, "黒い文字", "black")
Call viewWait(IE, result, "赤い文字", "red")
Call waitClose(IE, "終了", 5) 'IE,メッセージ,秒

'*** ↓ IE message ******************************************
Set IE = Nothing
'*** ↑ IE message ******************************************

'*** ↓ IE message ******************************************
'IEを開いてメッセージ表示
Private Sub viewWait(ByVal IE, ByVal result, ByVal msg, ByVal col)
    Dim strMsg
    'エラーの場合のメッセージを作る
    If result <> 0 Then
        strMsg = "ErrorCode:" & result & "<br><br>" & msg
    Else
        strMsg = msg
    End If
    With IE
        If .Visible = False Then
            .Navigate "about:blank"
            While .busy: Wend
            While .Document.readyState <> "complete": DoEvents: Wend
            .Document.body.innerHTML = "<span id='msg' style='color:" & col & "'>" & strMsg & "</span>"
            .AddressBar = False
            .Toolbar = False
            .StatusBar = False
            .Height = 150
            .Width = 300
            .Top = 0
            .Left = 0
            .Visible = True
        Else
            'IE.Document.getElementById("msg").innerHTML = msg
            .Document.getElementById("msg").innerHTML = "<span id='msg' style='color:" & col & "'>" & strMsg & "</span>"
        End If
    End With
    'IEを前面に
    Call GetProcID(IE)
    CreateObject("Wscript.Shell").AppActivate intProcID
End Sub
'****************************************************************************************
'IEにメッセージを表示してから指定秒後に削除する
Private Sub waitClose(ByVal IE, ByVal msg, ByVal T)
On Error Resume Next
    IE.Document.getElementById("msg").innerHTML = msg
    WScript.Sleep T * 1000
    IE.Quit
End Sub
'****************************************************************************************
Private Function GetProcID(ByVal ProcessName)
    Dim Service
    Dim QfeSet
    Dim Qfe
    
    Set Service = WScript.CreateObject("WbemScripting.SWbemLocator").ConnectServer
    Set QfeSet = Service.ExecQuery("Select * From Win32_Process Where Caption='" & ProcessName & "'")
 
    intProcID = 0
 
    For Each Qfe In QfeSet
        intProcID = Qfe.ProcessId
        Exit For
    Next
 
    GetProcID = intProcID <> 0
    
End Function
'*** ↑ IE message ******************************************