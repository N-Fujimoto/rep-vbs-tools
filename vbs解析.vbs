Option Explicit
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
'ソースコード解析ツール
'2009/02/18 N-FUJIMOTO
'2022/07/27  N-FUJIMOTO 
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
    Private wshName
	wshName = Wscript.ScriptName
'定数の宣言
    Private Const ForReading = 1    '読み込み
    Private Const ForWriting = 2    '書きこみ（上書きモード）
    Private Const ForAppending = 8  '書きこみ（追記モード）
    Private Const sep = "'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*"    
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
'メイン
    Call main
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
'テスト
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
Public Sub test()
   Call main
   
End Sub
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
'メイン
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
Private Sub main()
On Error Resume Next
    Dim objArgs
    Dim fso
    Dim fileName
    Dim j
	Dim ret
	Dim i
'test    wshName = ActiveWorkbook.Name 'test
'引数はVBSファイル
    Set objArgs = Wscript.Arguments
'ファイルSYS
	Set fso = CreateObject("Scripting.FileSystemObject")
'NF210219
    For i = 0 To objArgs.Count - 1
'ファイル名
		fileName = objArgs(i)
'test    fileName = "F:\ＶＢ版製作中\新月報システムＤＢ版\unyouMaintenance.vbs" 'test
	    If Not fso.fileExists(fileName) Then
	    'ファイルの確認1
	       MsgBox "ファイルがドロップされていません", vbExclamation, wshName
	       Exit Sub
	    End If
'ﾊﾟｽの切り分け
	    j = Split(fileName, "\")
	    If Not StrComp(Right(j(UBound(j)), 4), ".VBS", vbTextCompare) = 0 Then
	    'ファイルの確認2
	       MsgBox j(UBound(j)) & vbCrLf & "...はVBSではありません。", vbExclamation, wshName
	       'Exit Sub
	    End If
		If objArgs.Count - 1 > 0 then
		   Call createKaiseki(fileName,1)	   
		Else   
		   ret = MsgBox(j(UBound(j)) & vbCrlf & "...を解析しますか？",vbYesNo+vbQuestion,wshName)
	       If ret = vbYes then 
		      Call createKaiseki(fileName,0)	   
		   End If
	    End If
	Next
	If objArgs.Count - 1 > 0 then
       Call MsgBox("解析結果を以下の場所に作成しました。" & String(2,vbCrlf) & _
	               GetMyPath & "資料\", vbInformation, wshName)	
    End If			   
	Set fso = Nothing
	Set objArgs = Nothing
    
End Sub
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
'解析
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
Private Sub createKaiseki(ByVal fileName,ByVal iFlg)
On Error Resume Next
    Dim fso 'FILESYS
    Dim fi  'インプット（VBS）
    Dim fo  'アウトプット（解析TXT）
    Dim j   'パス切り分け用
    Dim strRecord 'インプットレコード
    Dim bolflg_Titie 'タイトルを読み取れたか
    Dim bolflg_Proc  'プロシージャタイトルを読み取れたか
    Dim strProcTitle 'プロシージャのタイトルを保管
    Dim intLen       '文字列がHITした場所
    Dim i            '添え字
	Dim strOutFile    '解析ファイル名
	Dim strWK        'NF210219
'ファイルSYS
    Set fso = CreateObject("Scripting.FileSystemObject")
'ﾊﾟｽの切り分け
    j = Split(fileName, "\")
'解析ファイルを生成
    strOutFile = "解析_" & Mid(j(UBound(j)), 1, Len(j(UBound(j))) - 4) & ".txt"
    if not (fso.folderExists(fso.buildpath(GetMyPath,"資料"))) then
    	fso.CreateFolder(fso.buildpath(GetMyPath,"資料"))
    end if
	Set fo = fso.OpenTextFile(GetMyPath & "資料\" & strOutFile, ForWriting, True)
'インプット
    Set fi = fso.OpenTextFile(fileName)
'ファイルの中身を見る
    Do Until fi.AtEndOfStream
        strRecord = fi.ReadLine
'----------------------------------------------------------------------------------------
'コメント行から、プロシージャのタイトルとサブルーチンの詳細を取得する
'----------------------------------------------------------------------------------------
        If Left(strRecord, 1) = "'" Then
         '最初のコメントをVBSのタイトルとする
           If Not bolflg_Titie Then
              If Asc(Left(Replace(strRecord, "'", ""), 1)) < 0 Then
                 Call subWrite(fo, sep)
              'プロシージャのタイトルは日本語で始まるｗ
                 Call subWrite(fo, "VBS:" & Trim(Mid(strRecord, 2)))
              'タイトルとして任命ｗ
                 bolflg_Titie = True
              End If
           Else
'----------------------------------------------------------------------------------------
'各種コードからはSUBやFUNCTIONのみを取得
'----------------------------------------------------------------------------------------
           'プロシージャのタイトルを確認
              If Not bolflg_Proc Then
              'まだタイトルを奪取してないｗ
                 If Asc(Left(Replace(strRecord, "'", ""), 1)) < 0 Then
                 'プロシージャのタイトルは日本語で始まるｗ
                    strProcTitle = Trim(Mid(strRecord, 2))
                    'bolflg_Proc = True
                 End If
              End If
           End If
        Else
        'プロシージャの中身を判定
           If InStr(1, LCase(strRecord), "option explicit", vbTextCompare) > 0 Or _
              InStr(1, LCase(strRecord), "On Error", vbTextCompare) > 0 Then
           'Option Explicitを見つけた！
           'On Errorがあるか
              Call subWrite(fo, vbTab & strRecord)
           ElseIf InStr(1, LCase(strRecord), " sub ", vbTextCompare) > 0 Or _
                  InStr(1, LCase(strRecord), " function ", vbTextCompare) > 0 Then
              Call subWrite(fo, sep)
           'サブルーチンを見つけた！
              'Private Function getMSG ByVal strMsg, ByVal strTitle)
           'プロシージャタイトルを設置
              Call subWrite(fo, "Proc:" & strProcTitle)
           'プロシージャ名と引数を分離
              j = Split(strRecord, "(") '引数との分離
              Call subWrite(fo, vbTab & j(0))
           '引数がある場合はArgsとして設置
              If UBound(j) > 0 Then
                 strWK = Replace(j(1), ")", "") '[)]カット NF210219
                 If Len(strWK) > 0 Then 'NF210219	
                 '引数なしは省略
                    j = Split(strWK, ",") '引数をSPLIT NF210219
                    For i = LBound(j) To UBound(j)
                        If i = 0 Then
                        '最初だけ見出しあり
                           Call subWrite(fo, String(2, vbTab) & "Args:" & j(i))
                        Else
                        '２回目以降見出しなし
                           Call subWrite(fo, String(2, vbTab) & "    " & j(i)) 'NF210219
                        End If
                    Next
                 End If
              End If
           End If
        End If
    Loop
'破棄
    If Not fi Is Nothing Then fi.Close
    Set fi = Nothing
    If Not fo Is Nothing Then fo.Close
    Set fo = Nothing
	If iFlg = 0 Then
       Call MsgBox("解析結果を以下の場所に作成しました。" & String(2,vbCrlf) & _
	               GetMyPath & "資料\" & vbCrlf & _
			       "┗ " & strOutFile, vbInformation, wshName)
    End If
	
 End Sub
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
'書き込みサブルーチン
'*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*^-^*
Private Sub subWrite(ByVal fo, ByVal strRecord)
    fo.Write strRecord & vbCrLf
    
End Sub
'*******************************************************************************
'自分自身のパスを取得モジュール
'*******************************************************************************
Function GetMyPath()
'test    GetMyPath = "F:\ＶＢ版製作中\新月報システムＤＢ版\" 'test
'最後の\サインは着いています
    Dim strFullPath
    Dim strFileName
    With Wscript
       strFullPath = .ScriptFullName
       strFileName = .ScriptName
       GetMyPath = Mid(strFullPath, 1, Len(strFullPath) - Len(strFileName))
    End With
    
End Function