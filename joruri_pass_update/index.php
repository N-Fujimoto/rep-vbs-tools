<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Joruri Mail Password変更</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<?php
    header("Content-type: text/html; charset=utf-8");
    $errors = array(); //トラップされる
    $info = array();   //トラップされない
    if(isset($_POST['submit']) && $_POST['submit'] === "変更"){
        $account = $_POST['account'];
        $oldpassword = $_POST['oldpassword'];
        $newpassword = $_POST['newpassword'];
        $chkpassword = $_POST['chkpassword'];
        // validation
        if($account === ""){
            $errors['account'] = "ユーザIDが入力されていません.";
        } else if ($newpassword === "" && $chkpassword === ""){
            $info['nullpassword'] = "パスワードは空で設定されます.";
        }
        /*
        if($oldpassword === ""){
            $errors['oldpassword'] = "現在のパスワードが入力されていません.";
        }
        */
        if ($newpassword === "" && $chkpassword === ""){
            //$info['nullpassword'] = "パスワードは空で設定されます.";
        } else {
            if($newpassword === ""){
                $errors['newpassword'] = "新しいパスワードが入力されていません.";
            } else if($chkpassword === ""){
                $errors['chkpassword'] = "確認用パスワードが入力されていません.";
            } else if ($newpassword !== $chkpassword){
                $errors['notpassword'] = "新しいパスワードと確認用パスワードが一致しません.";
                // 新パスワードと確認用パスワードが違う場合のみクリア
                unset($newpassword);
                unset($chkpassword);    
            }
        }
        // エラーがなければ次へ
        if(count($errors) === 0){
        	// ユーザが存在するかチェック
        	system("perl /usr/local/mail_admin_tool/joruri_pwd_check.pl $account $oldpassword &> /dev/null" , $status);
        	if($status != 0)
        	{
        		$errors['notaccount'] = "「ユーザID」あるいは「現在のパスワード」が誤っています.";
        	} else {
        	    //更新を行うチェック
        	    system("perl /usr/local/mail_admin_tool/joruri_pwd_update.pl $account $newpassword &> /dev/null",$status);
	            if($status != 0)
        	    {
        	        $errors['notmodify'] = "パスワード更新に失敗しました.システム管理者にお問い合せください.";
        	    } else {    
                    //echo "パスワードは変更されました.<a href='http://omecvsv00231.matsusaka.co.jp/' target='_self'>ログイン</a>ページに戻る.";
                    echo "パスワードは変更されました.メニューの[Webメール]をクリックしてください.<br><br>";
                    //unset($account);
                    unset($oldpassword);
                    unset($newpassword);
                    unset($chkpassword);
                }
            }
        }
    }
?>
<form action="index.php" method="post">
    <table>
    	<tr>
    		<td>ユーザID</td>
    		<td><input type="text" name="account" maxlength="32" size="20" value="<?php if(isset($account)){ echo $account; } ?>"></td>
    	</tr>
    	<tr>
    		<td>現在のパスワード</td>
    		<td><input type="password" name="oldpassword" maxlength="8" size="10" value="<?php if(isset($oldpassword)){ echo $oldpassword; } ?>"></td>
    	</tr>
    	<tr>
    		<td>新しいパスワード</td>
    		<td><input type="password" name="newpassword" maxlength="8" size="10" value="<?php if(isset($newpassword)){ echo $newpassword; } ?>"></td>
    	</tr>
    	<tr>
    		<td>新しいパスワード（確認）</td>
    		<td><input type="password" name="chkpassword" maxlength="8" size="10" value="<?php if(isset($chkpassword)){ echo $chkpassword; } ?>"></td>
    	</tr>
    	<tr>
    		<td><input type="submit" name="submit" value="変更"></td>
    	</tr>
    </table>
    <?php
        echo "<span style='color:red'><ul>";
        foreach($errors as $message){
            echo "<li>"; 
            echo $message;
            echo "</li>"; 
        }
        echo "</ul></span>";
        echo "<span style='color:blue'><ul>";
        foreach($info as $message){
            echo "<li>"; 
            echo $message;
            echo "</li>"; 
        }
        echo "</ul></span>"; 
    ?>
</form>
</body>
</html>
