use DBI;
#--------------------------------------------------------------------------------------------------#
# jorurimailのユーザパスワードを変更する
#  2016/05/26  N-FUJIMOTO 
# 前提
#  1.mysqlサーバに外部接続用のユーザを定義してください
#    例)grant all privileges on *.* to user@'%' identified by 'mec-L0c@L';
#
# 使用方法
#  system("/usr/bin/sudo /[設置パス]/joruri_pwd_update.pl [アカウント] [新パスワード] &> /dev/null",$status);
#  /usr/local/mail_admin_tool
#--------------------------------------------------------------------------------------------------#
# Setting
#--------------------------------------------------------------------------------------------------#
# mysqlホスト名
my $h = 'omecvsv00231.matsusaka.co.jp';
# データベース名
my $d = 'jorurimail_production';
# ユーザ名（前提参照）
my $u = 'user';
# パスワード（前提参照）
my $p = 'mec-L0c@L';
# ldap値[0]連携なし[1]連携あり
my $l = 0;
# 引数受付
my $account 	= $ARGV[0];
my $new_password= $ARGV[1];
#--------------------------------------------------------------------------------------------------#
# Sample
# | id  | air_login_id | state   | created_at          | updated_at          | ldap | ldap_version | auth_no | name                  | name_en | account  | password  | mobile_access | mobile_password | email                         | remember_token | remember_token_expires_at | kana | sort_no | official_position | assigned_job | group_s_name |
# +-----+--------------+---------+---------------------+---------------------+------+--------------+---------+-----------------------+---------+----------+-----------+---------------+-----------------+-------------------------------+----------------+---------------------------+------+---------+-------------------+--------------+--------------+
# |   1 | NULL         | enabled | 2016-05-12 19:10:41 | 2016-05-13 10:03:51 |    0 | NULL         |       5 | システム管理者        |         | admin    | mec-L0c@L |             1 | mec-L0c@L       | admin@matsusaka.co.jp         | NULL           | NULL                      |      |         |                   |              | NULL         |
#--------------------------------------------------------------------------------------------------#
# 年月日時分秒を作成 => yyyy-mm-dd hh:mm:ss
my ($sec,$min,$hour,$day,$mon,$year,$wdy,$yday,$isdst) = localtime(time);
my $nitiji = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $year + 1900, $mon +1, $day,$hour,$min,$sec);
# クエリ sys_users を更新する
my $qry = "
   UPDATE sys_users 
   SET password   = \'$new_password\',
       updated_at = \'$nitiji\',
       ldap       = $l
   WHERE account = \'$account\';";
# データベースへ接続
my $db = DBI->connect("DBI:mysql:".$d.";host=$h", $u,$p) || die "Error[$!]\n";
# 更新クエリ発行
my $sth = $db->prepare($qry);
$sth->execute;
## 2016/05/27  N-FUJIMOTO test s
# # 更新テスト
# $qry = "select account,password,updated_at,ldap from sys_users WHERE account = \'$account\';";
# # 参照クエリ発行
# $sth = $db->prepare($qry);
# $sth->execute;
# while ( my $row_ref = $sth->fetch ) { # fetch は fetchrow_arrayref の alias
# 	foreach my $i (0 .. @$row_ref-1) {
# 		print "$row_ref->[$i] ";
# 	}
# 	print "\n";
# }
## 2016/05/27  N-FUJIMOTO test e
$rc = $sth->finish;
$rc = $db->disconnect;

exit;

