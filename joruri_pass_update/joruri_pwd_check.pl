use DBI;
#--------------------------------------------------------------------------------------------------#
# jorurimailのユーザが存在するかチェックする
#  2016/06/08  N-FUJIMOTO 
# 前提
#  1.mysqlサーバに外部接続用のユーザを定義してください
#    例)grant all privileges on *.* to user@'%' identified by 'mec-L0c@L';
#
# 使用方法
#  system("/usr/bin/sudo /[設置パス]/joruri_pwd_check.pl [アカウント] [現パスワード] &> /dev/null",$status);
#  /usr/local/mail_admin_tool
#--------------------------------------------------------------------------------------------------#
# Setting
#--------------------------------------------------------------------------------------------------#
# mysqlホスト名
my $h = 'omecvsv00231.matsusaka.co.jp';
# データベース名
my $d = 'jorurimail_production';
# ユーザ名（前提参照）
my $u = 'user';
# パスワード（前提参照）
my $p = 'mec-L0c@L';
# ldap値[0]連携なし[1]連携あり
my $l = 0;
# 引数受付
my $account 	= $ARGV[0];
my $old_password= $ARGV[1];
#--------------------------------------------------------------------------------------------------#
# Sample
# | id  | air_login_id | state   | created_at          | updated_at          | ldap | ldap_version | auth_no | name                  | name_en | account  | password  | mobile_access | mobile_password | email                         | remember_token | remember_token_expires_at | kana | sort_no | official_position | assigned_job | group_s_name |
# +-----+--------------+---------+---------------------+---------------------+------+--------------+---------+-----------------------+---------+----------+-----------+---------------+-----------------+-------------------------------+----------------+---------------------------+------+---------+-------------------+--------------+--------------+
# |   1 | NULL         | enabled | 2016-05-12 19:10:41 | 2016-05-13 10:03:51 |    0 | NULL         |       5 | システム管理者        |         | admin    | mec-L0c@L |             1 | mec-L0c@L       | admin@matsusaka.co.jp         | NULL           | NULL                      |      |         |                   |              | NULL         |
#--------------------------------------------------------------------------------------------------#
# クエリ sys_users を参照する
my $qry = "
	Select account,password,updated_at,ldap From sys_users Where account = \'$account\'
	and password = \'$old_password\';";
# データベースへ接続
my $db = DBI->connect("DBI:mysql:".$d.";host=$h", $u,$p) || die "Error[$!]\n";
# 参照クエリ発行
$sth = $db->prepare($qry);
$sth->execute;
$num = $sth->rows;
$rc = $sth->finish;
$rc = $db->disconnect;
# レコードがなければ999を返す
# print $num;
if ($num == 0) {
	return 999;
}
exit;

