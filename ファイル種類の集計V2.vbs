option explicit
'============================================================
'ROBOCOPYの結果から拡張子別のファイル部分布資料を作成する
'2020/09/29  N-FUJIMOTO V2
'
'
'解析するフォルダ
Private arg
'============================================================

if Wscript.Arguments.length = 0 then
	msgbox "解析するフォルダをドロップしてください.",vbInformation,"ファイル解析"
	Wscript.quit
end if

dim ret
arg = Wscript.Arguments(0)
ret = msgbox("【 " & arg & " 】フォルダを解析します...よろしいですか？",vbQuestion + vbYesNo,"ファイル解析")

if ret = vbyes then 
	call main(arg)
end if

'メイン
Private sub main(byval strPath)
	dim objFso,myPath,objFile,iFile,objFolder,strLine,ret,arr,oLog,objRS,wk,iSize,objADO,midFile,oFile,strSQL,fdr
	
	wk = split(strPath,"\")	'パスを分割
	fdr = wk(ubound(wk))	'パス名
	Set objFso = CreateObject("Scripting.FileSystemObject")
	'カレントディレクトリ
	'myPath = objFso.GetFolder(".").Path
	myPath = ModulePath() 'EXE化に伴う変更
	'解析するフォルダオブジェクト
	'Set objFolder = objFso.GetFolder(objFso.BuildPath(myPath,fdr))
	Set objFolder = objFso.GetFolder(strPath)	'フルパス
	'ファイルが無いなら終わり
	IF objFolder.Files.Count = 0 then
		set objFolder = Nothing
		Set objFso = Nothing
	    Exit sub
	End IF
	

	midFile = "中間ファイル(" & wk(ubound(wk)) & ").csv"
	'解析用中間ファイルを作成する
	Set oLog = objFso.OpenTextFile(objFso.BuildPath(myPath, midFile), 2, True)
	'ヘッダ記載
	oLog.WriteLine "filename,ext,size,logname"
	'フォルダ内のファイルをチェック
	For Each objFile In objFolder.Files
	    if LCase(objFso.GetExtensionName(objFile.name)) = "log" then
	    	
			Set iFile = objFso.OpenTextFile(objFile.Path, 1, False)
			
			iFile.Skip 15 '効かない？
			Do Until iFile.AtEndOfStream
		    	strLine = iFile.ReadLine
'on error resume next 'test
		    	arr = Split(strLine,vbTab) 'タブ区切り
		    	if UBound(arr) > 2 then
			    	if instr(arr(1),"同じ") > 0 or instr(arr(1),"更新済み") > 0 or instr(arr(1),"新しいファイル") > 0 then
				    	if instr(arr(4),".") > 0 then	'拡張子ありだけ
				    		wk = split(arr(4),".")
				    		if instr(arr(3)," k") > 0 then
				    		'キロバイト⇒バイト
				    			arr(3) = CDbl(replace(arr(3)," k",""))
				    			iSize = arr(3) * 1024
				    		elseif instr(arr(3)," m") > 0 then
				    		'メガバイト⇒バイト
				    			arr(3) = CDbl(replace(arr(3)," m",""))
				    			iSize = arr(3) * 1024 * 1024
				    		elseif instr(arr(3)," g") > 0 then
				    		'ギガバイト⇒メガバイト⇒キロバイト⇒バイト
				    			arr(3) = CDbl(replace(arr(3)," g",""))
				    			iSize = arr(3) * 1024 * 1024 * 1024
				    		else
				    		'バイト
				    			iSize = CDbl(arr(3))
				    		end if
				    		'filename,ext,size,logname
				    		oLog.WriteLine replace(arr(4),",","，") & "," & UCase(wk(ubound(wk))) & "," & iSize & "," & objFile.name	'ファイルリスト用書き込み
						end if
					end if
				end if
			Loop
			iFile.close
			'exit for 'test
		end if
	Next
	oLog.Close
	
	'解析結果ファイルを作成する
	Set oFile = objFso.OpenTextFile(objFso.BuildPath(myPath,"解析結果【" & fdr & "】.csv"), 2, True)
	'ADOを使いCSVファイルを扱う準備
	Set objADO = CreateObject("ADODB.Connection")
	
	Call ADO_Open(objADO, myPath)
	'SQLを実行して、対象データを抽出してファイルに出力
	strSQL = "SELECT " & _
				"ext,count(ext) as ct,sum(size) as sumsize,min(size) as minsize,max(size) as maxsize,avg(size) as avgsize" & _
				" FROM [" & _
				midFile & "] group by ext order by ext;"
	Set objRS = objADO.Execute(strSQL)
	'レコードセットを先頭行に移動
	objRS.MoveFirst
	'ヘッダ記載
	oFile.WriteLine "拡張子,件数,合計(B),最小(B),最大(B),平均(B)"
	Do While Not objRS.EOF
		oFile.WriteLine _
			objRS("ext").Value & "," & _
			objRS("ct").Value & "," & _
			objRS("sumsize").Value & "," & _
			objRS("minsize").Value & "," & _
			objRS("maxsize").Value & "," & _
			objRS("avgsize").Value
	    objRS.MoveNext
	Loop
	objRS.Close
	oFile.Close
	
	'オブジェクトのクローズ＆破棄
	Set iFile = Nothing
	set oLog = Nothing
	Set oFile = Nothing
	Set objFso = Nothing
	Set objRs = Nothing
	objADO.Close
	Set objADO = Nothing
	
	msgbox "完了",vbInformation,"ファイル解析"
	
end sub

'***********************************************************************************************
'ADODBドライバオープン
'変な仕様なんだが
'	64ビットOSの場合でかつ、VBSで実行する場合は「AccessDatabaseEngine_X64.exe」をインストールが必要
'	64ビットOSの場合でも、EXE形式にすれば32ビット版ドライバが選択される様子
'2017/10/27  N-FUJIMOTO
'***********************************************************************************************
Private Sub ADO_Open(ByRef objADO, ByVal strDir)
    Set objADO = CreateObject("ADODB.Connection")

On Error Resume Next

	'先に64ビット版ドライバを読み込んでおく
    objADO.Open "Driver={Microsoft Access Text Driver (*.txt, *.csv)};" & _
                "DBQ=" & strDir & ";" & _
                "ReadOnly=0"

	'エラーなら64ビット対応ドライバを読み込めないので32ビット版ドライバを読み込む
    If Err Then
        Err.Clear
        '32ビット版ドライバ
        objADO.Open "Driver={Microsoft Text Driver (*.txt; *.csv)};" & _
                    "DBQ=" & strDir & ";" & _
                    "ReadOnly=0"
        
    End If
    'ドライバを読み込めなかったら即終了！
    If Err Then
        Err.Clear
        MsgBox "テキストドライバのオープンに失敗しました.システム管理者に問い合わせてください.", vbCritical, _
               "ADODOドライバエラー"
        WScript.Quit
    End If
    
End Sub
'***********************************************************************************************
'パスの検証
'***********************************************************************************************
Function ModulePath()
  Dim T, fHandle, i, arr
  ModulePath = WScript.ScriptFullName
  T = LCase(ModulePath)
  T = Left(T, Len(T) - 4)
  If Right(T, 4) <> ".tmp" Then
    arr = Split(ModulePath, "\")
    ModulePath = ""
    For i = 0 To UBound(arr) - 1
        ModulePath = ModulePath & arr(i) & "\"
    Next
    Exit Function
  End If
On Error Resume Next
  Set fHandle = CreateObject("Scripting.FileSystemObject").OpenTextFile(T)
  T = fHandle.ReadLine
  fHandle.Close
  If Err.Number = 0 Then
    'ModulePath=T
    arr = Split(T, "\")
    ModulePath = ""
    For i = 0 To UBound(arr) - 1
        ModulePath = ModulePath & arr(i) & "\"
    Next
  End If
  
End Function