Option Explicit

Const ForReading = 1
Const ForWriting = 2

Dim objFSO, objFile, objNewFile
Dim strSourceFile, strDestinationFile, strLine, arrColumns

strSourceFile = "C:\source.csv"
strDestinationFile = "C:\destination.csv"

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile(strSourceFile, ForReading)

Set objNewFile = objFSO.CreateTextFile(strDestinationFile)

Do Until objFile.AtEndOfStream
    strLine = objFile.ReadLine
    arrColumns = Split(strLine, ",")
    '保険者番号,被保険者証番号,個人番号,性別,生年月日
    objNewFile.WriteLine arrColumns(2) & "," & arrColumns(4) & "," & arrColumns(6) & "," & arrColumns(8) & "," & arrColumns(41)
Loop

objFile.Close
objNewFile.Close

Set objFSO = Nothing
