option Explicit

'****************************************************************************************
'	DECOの短縮URLを若干楽に開く
'	2018/12/13  N-FUJIMOTO 
'	短縮URL部分を入力してIEで開く処理
'****************************************************************************************

const url="http://url.matsusaka.co.jp/s/*****"

call main

private sub main()
	dim ret,site
	
	ret = inputbox("短縮URLを入力します," & string(2,vbcrlf) & _
	               "http://url.matsusaka.co.jp/s/[この部分だけ入力]","短縮URL入力")
	
	'入力チェック（簡易）
	if ret = vbNullString then
		'call msgbox("5桁の短縮URL部分を入力してください.",vbCritical,"入力チェック")
	elseif not (len(ret)=5) then
		call msgbox("入力した値が不正です." & string(2,vbcrlf) & _ 
		            ret,vbCritical,"入力チェック")
	else
		site = replace(url,"*****",ret)
		call ie(site)
	
	end if
end sub

private sub ie(byval site)
	Dim objIE,ret
 
    'オブジェクトの作成
    Set objIE = CreateObject("InternetExplorer.Application")
    
    '指定したURLを開く
    objIE.Navigate2 site

	'ページが読み込まれるまで待つ
    Do While objIE.Busy = True Or objIE.readyState <> 4
        WScript.Sleep 100
    Loop

on error resume next

	with objIE.Document
	'生成ページに行かせないための処置
		if .getElementById("url") is Nothing then
			err.clear
			'IEを表示させる。Trueで表示
    		objIE.Visible = True 
		else
			objIE.quit
			set objIE = Nothing
			call msgbox("入力したURLは存在しませんでした." & string(2,vbcrlf) & _
			            site,vbCritical,"入力エラー")
		end if
	end with

end sub