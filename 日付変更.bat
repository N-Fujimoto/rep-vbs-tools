@echo off
cls

set DDA=1976/01/05
set DTI=05:05:05
echo;
echo 作成日時,更新日時,アクセス日時のうち、変更するものを選んでください。
echo (eg. 100=作成日時、001＝アクセス日時、未入力＝111=すべて）
set /p ITM=変更対象入力 : 
if "%ITM%1"=="1" set ITM=111

echo;
echo フォーマットに従い日付を入力してください。
echo  (eg. %DDA%  (←未入力はこの日付に設定)) ：
set /p DA=日付入力 : 
if "%DA%1"=="1" set DA=%DDA%
echo;
echo %DA%

echo;
echo フォーマットに従い日時を入力してください。
echo  (eg. %DTI%  (←未入力はこの時間に設定)) ：
set /p TI=時間入力 : 
if "%TI%1"=="1" set TI=%DTI%
echo;
echo %TI%
echo;

:SYORI
if "%ITM:~0,1%"=="1" powershell -ExecutionPolicy RemoteSigned Set-ItemProperty '%1' -name CreationTime -value '%DA% %TI%'
if "%ITM:~1,1%"=="1" powershell -ExecutionPolicy RemoteSigned Set-ItemProperty '%1' -name LastWriteTime -value '%DA% %TI%'
if "%ITM:~2,1%"=="1" powershell -ExecutionPolicy RemoteSigned Set-ItemProperty '%1' -name LastAccessTime -value '%DA% %TI%'

:GONEXT
shift
if /i "1%~1" NEQ "1" goto SYORI

:TERMINATION
echo 処理を終了しました。
timeout /t 10
endlocal