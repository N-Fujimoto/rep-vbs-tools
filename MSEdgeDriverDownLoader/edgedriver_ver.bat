@echo off
::+--------------------------------------------------------------------------+::
:: 現在のEdgeのバージョンと同じMSEdge Driverを取得してくるやーつ
:: 2022/10/24  N-FUJIMOTO 
::+--------------------------------------------------------------------------+::
::+設定開始
::+Driver設置先を設定します
rem set dst=C:\Users\100553\Documents\rep-python_prod\deco\driver
set dst=%~dp0
set dri=edgedriver_win64
::+設定終わり
::+--------------------------------------------------------------------------+::
::
::
::
::+--------------------------------------------------------------------------+::
setlocal enabledelayedexpansion
cd /d %~dp0
set timeout=60
::+--------------------------------------------------------------------------+::
::現在のMSEdgeのバージョンを調査
powershell -Command (Get-ItemProperty '%programfiles(x86)%\Microsoft\Edge\Application\msedge.exe').versioninfo.fileversion > ./edge_nver.txt
for /f "tokens=1,* delims==" %%a in (./edge_nver.txt) do set nver=%%a
for /f "tokens=1,3 delims=:. " %%a in ("%nver%") do set mver=%%a
echo インストールされているMSEdgeのバージョン：%nver%(%mver%)
echo.
::+--------------------------------------------------------------------------+::
::最終リリースのバージョンを取得
set url=https://msedgewebdriverstorage.blob.core.windows.net/edgewebdriver/LATEST_RELEASE_%mver%
curl -s %url% > ./edge_nver.txt
::バッチで読み込むためUTF-16BEでダウンロードされているファイルをS-JISに変換する
cscript //nologo bin/chjis.vbs ./edge_nver.txt ./edge_lver.txt
for /f "tokens=1,* delims==" %%a in (./edge_lver.txt) do (
	set lver=%%a
)
echo リリースされている最新ドライバのバージョン：%lver%
echo.
erase edge_nver.txt
erase edge_lver.txt
::+--------------------------------------------------------------------------+::
::+ダウンロード
echo %dri%.zip(%lver%) をダウンロードしています...
set url=https://msedgewebdriverstorage.blob.core.windows.net/edgewebdriver/%lver%/%dri%.zip
curl -s %url% -o %dri%.zip
echo.
::+--------------------------------------------------------------------------+::
::zipを解凍して設置先へコピー
echo %dri%.zip(%lver%) を解凍しています...
powershell Expand-Archive -Path %dri%.zip -Destination %dst% -Force
echo.
erase %dri%.zip
rmdir /S /Q %dst%Driver_Notes
color 0b
echo [%dst%]への設置は完了しました(%timeout%秒待機します)
timeout /t %timeout%
goto :EOF
