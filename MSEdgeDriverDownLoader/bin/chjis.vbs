inpf=WScript.Arguments(0)
outf=WScript.Arguments(1)

Set objStream = WScript.CreateObject("adodb.stream")
objStream.Type = 2
objStream.Charset = "UTF-16"
objStream.Open
objStream.LoadFromFile(inpf)     ' UTF-16 のファイルを読み込み

txt = objStream.ReadText()

objStream.Close()


objStream.Open()
objStream.Position = 0
objStream.Charset = "Shift_JIS"         ' Shift-JIS でファイルへ書き込み
objStream.WriteText txt

objStream.SaveToFile outf,2 '上書き

objStream.Close
Set objStream = Nothing