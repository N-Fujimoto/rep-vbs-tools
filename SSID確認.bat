::*****************************************************************
::　バッチファイル
::*****************************************************************
@echo off
:startssidmgr
set NUM=
set SSID=
set RSID=
set IFNAME=
set PRIORITY=
set Rtry=
::CLS
echo ----------------メニュー----------------
echo [1] 登録済み無線APの優先順位を確認
echo [2] 無線APの優先順位の変更
echo [3] 登録済みの無線LANプロファイル削除
echo [4] この処理を終了する
echo -----------------------------------------
set /p NUM="実行する処理が記載された番号を入力してください >"
if "%NUM%"=="1" goto showaplist
if "%NUM%"=="2" goto apmod
if "%NUM%"=="3" goto apDel
if "%NUM%"=="4" goto endMsg
goto NoNumber

:showaplist
cls
echo [1] 登録済み無線APの優先順位を確認
netsh wlan show profiles
echo .
echo 必要な情報はメモ帳などに転記してください。
echo .
pause
goto startssidmgr

:apmod
cls
echo [2] 無線APの優先順位の変更
set /p SSID="優先順位を変更するプロファイル名を入力します。 >"
set /p PRIORITY="このプロファイルの優先順位を数字で入れてください。 >"
:ExecSSIDmod
echo このホスト（%COMPUTERNAME%）で利用する無線LANプロファイル%SSID%の
echo 優先順位を%PRIORITY%に変更します。よろしいですか？
echo ※キャンセル時にはメインメニューに戻ります。
set /p Rtry="変更の実行は[Y]をキャンセルは[N]を入力しEnterキーを押下します。 >"
if /i %Rtry% == y (goto startSSIDmod) 
if /i %Rtry% == n (goto startssidmgr) 
echo 正しく入力してください。
goto ExecSSIDmod
:startSSIDmod
netsh wlan set profileorder name="%SSID%" interface="Wi-Fi" priority=%PRIORITY%
echo --------------------------------------------------
echo %SSID%のプロファイル順位が%PRIORITY%になりました。
echo --------------------------------------------------
goto end

:apDel
cls
echo [3] 登録済みの無線LANプロファイル削除
echo プロファイル名は「メニュー[1] 登録済み無線APの優先順位を確認」
echo から確認してください。
echo ...
set /p SSID="削除するプロファイル名を入力します >"
set /p RSID="確認のためもう一度入力してください >"
IF  %SSID%==%RSID% goto ExecDel
echo 入力に誤りがあります。入力を確認してください。
echo 再度入力画面に戻ります。
echo .
goto apDel
:ExecDel
echo このホスト（%COMPUTERNAME%）から無線LANプロファイル名"%SSID%"を削除します。
echo よろしいですか？　※キャンセル時にはメインメニューに戻ります。
set /p Rtry="削除実行は[Y]をキャンセルは[N]を入力しEnterキーを押下します。 >"
if /i %Rtry% == y (goto startapDel) 
if /i %Rtry% == n (goto startssidmgr) 
echo 正しく入力してください。
goto ExecDel
:startapDel
netsh wlan delete profile name="%SSID%"
echo ------------------------------------
echo %SSID%のプロファイル削除処理が完了しました。
echo ------------------------------------
goto end

:NoNumber
cls
echo ちゃんと指定された番号を入力してください。
goto startssidmgr
:interruption
echo 処理を中断しました。
:end
rem もう一度処理をするかどうか確認し、処理しない場合終了します。
echo --------------------------------------------------------------
echo 別のSSIDについて確認/編集/削除を実行するには[Y]を
set /p Rtry="処理を終了するには[N]を入力しEnterキーを押下します。 >"
if /i %Rtry% == y (goto startssidmgr) 
if /i %Rtry% == n (goto endMsg) 
goto endMsg
:endMsg
set NUM=
set SSID=
set RSID=
set IFNAME=
set PRIORITY=
set Rtry=
echo %0 の動作が完了しました。
::*****************************************************************