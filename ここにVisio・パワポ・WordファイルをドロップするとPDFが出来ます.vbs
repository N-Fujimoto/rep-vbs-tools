Option Explicit
'==========================================================================================
'ファイルやフォルダをドロップすれば再帰的にVisioのファイルをPDFに変換するツール
'2022/03/07  N-FUJIMOTO ファイルのみ版公開
'2022/03/08  N-FUJIMOTO ファイルのみ対応版からのアップデートして公開
'2022/03/11  N-FUJIMOTO ついでにパワポも対応
'==========================================================================================
'PDF化を除外するフォルダをカンマ区切りで指定します
const skipFolderName = "old,凍結,不要,削除"

'変数定義
Dim i,ret
Dim args
Dim fs,fldr,subdr,msg

Set args = wscript.Arguments
'ドロップしてないなら終わり
If args.Count = 0 Then 
	MsgBox "ここに『Visio』『パワポ』『Word』 または" & string(1,vbCrLf) &	_
			"『それらが格納されているフォルダ』をドロップしてください." & string(1,vbCrLf) & _
			"（複数可、フォルダの場合は再帰処理あり）",vbInformation,"ファイルドロップ確認"
	wscript.Quit
end if

'拡張子、フォルダファイルチェック、ファイルコピー
Set fs = CreateObject("Scripting.FileSystemObject")

For i = 0 To args.Count - 1
	If fs.FolderExists(args.Item(i)) Then
	'folder
		Set fldr = fs.GetFolder(args.Item(i))
		ret = getSubFolder(fldr)
		set fldr = Nothing
	ElseIf fs.FileExists(args.Item(i)) Then
	'file
		ret = createPDF(args.Item(i))
	End If
    if ret then
    '一度でも変換したなら
    	msg = True
    else
    	exit for 'VisioやPPTが無い場合はFalseで帰ってくる
    end if
Next
'オブジェクト破棄
Set args = Nothing
Set fs = Nothing

if msg then MsgBox "ドロップしたファイルと同じ場所にPDFファイルを生成しました." ,vbInformation,"作成完了!"

'フォルダ再帰処理
Private Function getSubFolder(fldr)
	Dim file
	dim skipName,i,flg
	For Each file In fldr.Files
        getSubFolder = createPDF(file.path)
    Next
    For Each file In fldr.subFolders
    'skipFolderNameに基づきスキップとする
		skipName = split(skipFolderName,",")
		flg = True
		for i = LBound(skipName) to UBound(skipName)
			if file.name = skipName(i) then
				'msgbox "[" & file.name & "]フォルダはスキップしました.",vbInformation,"スキップしたフォルダの案内"
				flg = false
				exit for
			end if
		next
		if flg then getSubFolder(file)
    Next
end Function
'==========================================================================================
'SharePointURL作成
'==========================================================================================
Private function ConvertDirectoryPath(path) 'as String
	dim strwk
	strwk = Replace(path, " ", "%20")
	strwk = Replace(strwk, "/", "\")
	ConvertDirectoryPath = Replace(strwk, "http:", "")
end Function
'==========================================================================================
'PDF作成サブルーチン
'==========================================================================================
Private Function createPDF(path) 'as Boolean
	'Visio印刷用定数
	const visFixedFormatPDF = 1
	const visDocExIntentPrint = 1
	const visPrintAll = 0
	const visWS = &H20000000 '最小化
	'パワポ用定数
	const ppSaveAsPDF = 32
	'word用定数
	const wdExportformatPDF = 17
	
	Dim ext, wf
	Dim objApp,objDoc
	
On Error Resume Next
    ext = fs.GetExtensionName(path) '拡張子
    
    select case ext
    'vsdxのみ起動
    	case "vsdx"
    		'Visio起動と確認
			Set objApp = CreateObject("Visio.Application")
			If Err <> 0 Then
			    MsgBox "Visioがインストールされている端末で実施してください.", vbCritical, "Err:" & Err.Number
			    createPDF = False
			    Exit Function
			End If
			objApp.Window.WindowState = visWS
			objApp.Visible = False '一瞬出るけど見せない設定
			Set objDoc = objApp.Documents.Open(path)
		    wf = Left(path, Len(path) - Len(ext)) & "pdf"
		    'PDFで前ページ印刷
		    objApp.ActiveDocument.ExportAsFixedFormat visFixedFormatPDF, wf, visDocExIntentPrint, visPrintAll
		    'エラーなら次へ
		    If Err <> 0 Then
		        MsgBox Err.Description, vbCritical, Err.Number
		        Err.Clear
		    End If
		    objDoc.Close
	    	objApp.Visible = True
			objApp.Quit
			createPDF = True

		case "ppt","pptx"
			'パワポ起動と確認
			Set objApp = CreateObject("PowerPoint.Application")
			If Err <> 0 Then
			    MsgBox "PowerPointがインストールされている端末で実施してください.", vbCritical, "Err:" & Err.Number
			    createPDF = False
			    Exit Function
			End If
			set objDoc = objApp.Presentations.Open(path,true,false,false)
			wf = Left(path, Len(path) - Len(ext)) & "pdf"
			Call objDoc.SaveAs(wf,ppSaveAsPDF,false) '3つ目の引数はTrueフォントを埋め込むかどうか
			'エラーなら次へ
		    If Err <> 0 Then
		        MsgBox Err.Description, vbCritical, Err.Number
		        Err.Clear
		    End If
			objApp.DisplayAlerts = false
			objDoc.close
			objApp.DisplayAlerts = true
			objApp.quit
			createPDF = True
		
		case "doc","docx"
			'word起動と確認
			Set objApp = CreateObject("Word.Application")
			If Err <> 0 Then
			    MsgBox "Wordがインストールされている端末で実施してください.", vbCritical, "Err:" & Err.Number
			    createPDF = False
			    Exit Function
			End If
			set objDoc = objApp.Documents.Open(path)
			wf = Left(path, Len(path) - Len(ext)) & "pdf"
			Call objDoc.ExportAsFixedFormat(wf,wdExportformatPDF,false) '3つ目の引数はTrueフォントを埋め込むかどうか
			'エラーなら次へ
		    If Err <> 0 Then
		        MsgBox Err.Description, vbCritical, Err.Number
		        Err.Clear
		    End If
			objApp.DisplayAlerts = false
			objDoc.close
			objApp.DisplayAlerts = true
			objApp.quit
			createPDF = True
			
	end select
	set objDoc = Nothing
	set objApp = Nothing

end Function