@echo off
::+--------------------------------------------------------------------------+::
::+ フォルダ名をクリップボードにコピー v1
::+ 2022/10/19  N-FUJIMOTO 
::+--------------------------------------------------------------------------+::
::+ 設定開始(sortは Dir /? でも見てください)
set para=/ad-h-s
set sort=n
::+ 設定終わり
set clipexe=C:\Windows\System32\clip.exe
set path=%~dp1
dir %path% /b %para% /o%sort% | %clipexe%
