Option Explicit
'■
'■ ExecelのAutoOpenを起動させずに開く
'■ 2023/03/30  N-FUJIMOTO 
'■

'■
'■ メイン処理
'■
Private ret
if WScript.arguments.count = 1 then
	Call main(WScript.arguments(0)) 
End If
'■
'■ メインルーチン
'■
Private Sub main(byval toolorg)
    Dim oXlsApp, oBook
    ' Excel起動
    Set oXlsApp = CreateObject("Excel.Application")
    If oXlsApp Is Nothing Then
        MsgBox "Excel起動失敗"
    Else
        With oXlsApp
            .Application.Visible = False
            .Application.DisplayAlerts = False
            .Application.EnableEvents = False 'Auto_Open無効
            'ベースのファイルを開く
            Set oBook = .Application.Workbooks.Open(toolorg)
        On Error Resume Next
            .Application.ActiveWindow.DisplayWorkbookTabs = True
        	if err <> 0 then err.clear
            WScript.Sleep (3000)
            '設定を戻しておく
            .Application.EnableEvents = True
            .Application.DisplayAlerts = True
            .Application.Visible = True
        End With
    End If
    Set oXlsApp = Nothing
    
End Sub
